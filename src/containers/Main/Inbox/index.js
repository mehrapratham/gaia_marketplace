import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, Dimensions, StatusBar } from 'react-native'
import { Icon, Header, Container, Content, Drawer } from 'native-base'
import { LinearGradient } from 'expo';
import { connect } from 'react-redux'
import ItemList from '../../../components/Lists/ItemList';
import { Actions } from 'react-native-router-flux';
import MenuList from '../../../components/Lists/MenuList';
import { InboxList } from '../../../actions/MarketPlace';
var { height, width } = Dimensions.get('window');

class Inbox extends React.Component {
    constructor() {
        super()
        this.state = {
          
        }
    }
    componentWillMount() {
		this.props.dispatch(InboxList()).then(res => {
            // console.log(res.data,"000000")
		})
	}
    closeDrawer = () => {
        this.drawer._root.close()
        this.setState({ isOpened: false })
    };
    openDrawer() {
        if (this.state.isOpened) {
            this.closeDrawer()
        } else {
            this.drawer._root.open()
            this.setState({ isOpened: true })
        }
    };
    render() {
        var inboxLists = this.props.MarketPlace.inboxList || []
        console.log(inboxLists,"89999999")
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                side="right"
                content={<View style={styles.contentOut}>
                    <MenuList onPressDone={this.closeDrawer.bind(this)} />
                </View>}
                onClose={() => this.closeDrawer()}
                tapToClose={true}
                openDrawerOffset={0.2}>
                <Container style={styles.main}>
                <Header style={{ height: 100, marginTop: -25, paddingTop: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0 }}>
                      <LinearGradient
                            colors={['#66e8dc', '#6580ea']}
                            style={styles.containers}
                            start={[0.1, 0.1]} end={[1, 0.3]}>
                            <View style={styles.mainView}>
                                <View style={styles.width20}></View>
                                    <View style={styles.title}>
                                    <Text style={styles.colors}>Inbox</Text>
                                </View>
                                <View style={styles.iconOut}>
                                    <Icon name="ios-cart-outline" style={styles.color} />
                                </View>
                            </View>
                        </LinearGradient>
                    </Header>
                    <Content>
                        <StatusBar barStyle='light-content' />
                        <View style={styles.drawerOut}>
                            <View style={styles.width50}>
                                <Text style={styles.textColor}>35 results</Text>
                            </View>
                            <View style={styles.width50}>
                                <TouchableOpacity onPress={this.openDrawer.bind(this)}><Text style={styles.drawerText}>SORT AND FILTER</Text></TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            {inboxLists.map((item, key) => {
                                return (
                                    <View style={styles.map} key={key}>
                                        <ItemList name={item.name} price={item.price} image={item.thumbfile} date={item.end_time} rarity={item.rarity} onPress={() => { Actions.InventoryDetail() }} activeOpacity={0.5} />
                                    </View>
                                )
                            })}
                        </View>
                    </Content>
                </Container>
            </Drawer>
        )
    }
}

export default connect(state => ({}, mapDispatch))(Inbox);
const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}
const styles = StyleSheet.create({
    containers: {
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        marginTop: -25,
        height: 200,
        paddingTop: 0,
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 0
    },
    color: {
        color: '#fff',
        fontSize: 24,
        alignSelf: 'flex-end'
    },
    colors: {
        color: '#fff',
        fontSize: 18
    },
    mainView: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 34,
        paddingLeft: 10,
        paddingRight: 10,
     
    },
    flexOnes: {
        flex: 1
    },
    map: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    main: {

    },
    width20: {
        width: '20%',
    },
    iconOut: {
        width: '20%',
    },
    title: {
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    width50: {
        width: '50%'
    },
    textResults: {
        color: 'rgba(0,0,0,0.3)'
    },
    textDrawer: {
        color: '#6580ea',
        alignSelf: 'flex-end'
    },
    imageoutlines: {
        height: 70,
        width: 70,
        marginTop: height / 2 - 50,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: width / 2 - 35,
    },
    image: {
        width: '100%',
        height: '100%'
    },
    contentOut: {
        backgroundColor: '#fff',
        height: '100%',
        zIndex: 2
    },
    drawerOut: {
        width: '100%',
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    width50: {
        width: '50%'
    },
    drawerText: {
        color: '#6580ea',
        alignSelf: 'flex-end',
    },
    textColor: {
        color: 'rgba(0,0,0,0.3)'
    },
})