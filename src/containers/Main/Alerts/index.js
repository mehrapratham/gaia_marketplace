import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, StatusBar } from 'react-native'
import { Icon, Container, Header, Content } from 'native-base'
import { LinearGradient } from 'expo';
var { height, width } = Dimensions.get('window');
export default class Alerts extends React.Component {
    constructor() {
        super()
        this.state = {
            loader: true,
            item: [
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'

                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'SOLD',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Heart Container',
                    date: 'Jul 27, 2018'
                }
            ]
        }
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    render() {
        return (
            <Container style={styles.main}>
                <Header style={{ height: 100, marginTop: -25, paddingTop: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0 }}>
                    <LinearGradient
                        colors={['#66e8dc', '#6580ea']}
                        style={styles.containerss}
                        start={[0.1, 0.1]} end={[1, 0.3]}>
                        <View style={styles.mainView}>
                            <View style={styles.width20}></View>
                            <View style={styles.titleText}>
                                <Text style={styles.colors}>Alerts</Text>
                            </View>
                            <View style={styles.width20}>
                                <Icon name="ios-cart-outline" style={styles.color} />
                            </View>
                        </View>
                    </LinearGradient>
                </Header>
                <Content>
                    <StatusBar barStyle='light-content'/>
                    <View style={styles.map}>
                        {this.state.item.map((item, key) => {
                            return (
                                <View style={styles.imgout} key={key}>
                                    <View style={[styles.imgcont, { backgroundColor: this.getRandomColor() }]}>
                                        <View style={styles.imgmain}>
                                            <Image source={require('../../../img/heart.png')} style={styles.image} />
                                        </View>
                                    </View>
                                    <View style={styles.textout}>
                                        <Text style={styles.textDetails}>{item.name}</Text>
                                        <Text style={styles.textdetail}>{item.rarity}</Text>
                                        <Text style={styles.textdetail}>{item.date}</Text>
                                    </View>
                                    <View style={styles.touchableOut}>
                                        <View style={styles.priceOut}>
                                            <Text style={styles.price}>$ {item.dollar}</Text>
                                        </View>
                                    </View>
                                </View>
                            )
                        })}
                    </View>
                </Content>
            </Container>
        )
    }

}

const styles = StyleSheet.create({
    main: {
    },
    firstCon: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        paddingTop: 24,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center'
    },
    secondcon: {
        width: '20%',
    },
    text: {
        width: '60%',
        alignItems: 'center'
    },
    textmain: {
        fontSize: 18,
    },
    iconcart: {
        width: '20%',
        alignItems: 'flex-end'
    },
    textDetails: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingBottom: 4
    },
    priceOut: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    price: {
        fontSize: 16
    },
    scroll: {
        height: height
    },
    map: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    imgout: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)'
    },
    imgcont: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        width: '30%',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 10
    },
    imgmain: {
        height: 60,
        width: 60,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    textout: {
        width: '45%',
        paddingLeft: 10,
        justifyContent: 'center'
    },
    textdetail: {
        fontSize: 12,
        paddingBottom: 3,
        color: 'rgba(0,0,0,0.4)'
    },
    touchableOut: {
        width: '25%',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    icon: {
        fontSize: 22
    },
    priceOut: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    containerss: {
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
    color: {
        color: '#fff',
        alignSelf: 'flex-end',
        fontSize: 24
    },
    colors: {
        color: '#fff',
        fontSize: 18
    },
    mainView: {
        width: '100%',
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 34
    },
    width20: {
        width: '20%'
    },
    titleText: {
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageoutlines: {
        height: 70,
        width: 70,
        marginTop: height / 2 - 50,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: width / 2 - 35,
    },
    image: {
        width: '100%',
        height: '100%'
    },
})





