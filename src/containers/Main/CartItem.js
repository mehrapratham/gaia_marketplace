import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, Dimensions, StatusBar } from 'react-native'
import GradientButton from '../../components/Buttons/GradientButton'
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux'
import { CartItems } from '../../actions/MarketPlace'
import Headers from '../../components/Navigation/Headers'
import PopupComp from '../../components/Popup/Popup';
var { height, width } = Dimensions.get('window');

class CartItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showList: [],
            list: [],
            loader: false,
            btndisable: false
        }
    }
    componentDidMount() {
        if (this.props.CartData.cartItemList.length === 0) {
            this.popupOpen()
            console.log("hello")
            this.setState({ btndisable: true })
        }
        else {
            this.props.dispatch(CartItems('1234', 'show')).then(res => {
            })
        }
    }

    removeList(id) {
        this.setState({ loader: true })
        this.props.dispatch(CartItems(id, 'remove')).then(res => {
            this.setState({ loader: false })
        })
    }
    getTotalPrice() {
        let totalPrice = 0
        this.props.CartData.cartItemList.map(item => { totalPrice = totalPrice + parseInt(item.price) })
        return totalPrice
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    popupClose() {
        this.popupDialog.dismiss();
    }
    popupOpen() {
        this.popupDialog.show()
    }
    render() {
        let cartLists = this.props.CartData.cartItemList
        console.log(cartLists,"99999999")
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 6 }}>
                    <StatusBar barStyle="dark-content" />
                    <Headers lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Checkout" />
                    <ScrollView style={styles.scroll}>
                        <View style={styles.map}>
                            {cartLists.map((item, key) => {
                                return (
                                    <View style={styles.imgout} key={key}>
                                        <View style={[styles.imgcont, { backgroundColor: this.getRandomColor() }]}>
                                            <View style={styles.imgmain}>
                                                <Image source={{ uri: item.thumbfile }} style={styles.image} />
                                            </View>
                                        </View>
                                        <View style={styles.textout}>
                                            <Text style={styles.textdetail} numberOfLines={1}>{item.name}</Text>
                                            <Text style={styles.textdetail}>$ {item.price}</Text>
                                            <Text style={styles.textdetails}>Rarity: Ultra Rare</Text>
                                        </View>
                                        <View style={styles.touchableOut}>
                                            <TouchableOpacity style={styles.btnout} onPress={this.removeList.bind(this, item.id)} disabled={this.state.loader}>
                                                <Text style={styles.btntext}>{this.state.loader ? 'Loading' : 'Remove'}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            })}
                            <View style={styles.priceOut} >
                                <View style={styles.pricecont}>
                                    <Text style={styles.pricetext}>Total Price</Text>
                                </View>
                                <View style={styles.pricetextout}>
                                    <Text style={styles.pricetext}>$ {this.getTotalPrice()}</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.btncheckout} >
                    <GradientButton label="Checkout" disable={this.state.btndisable} onPress={() => { Actions.Payment({ cartLists }) }} />
                </View>
                <PopupComp link={(popupDialog) => { this.popupDialog = popupDialog; }}
                    onPress={this.popupClose.bind(this)} label='Error' description="Your cart is empty. Please select items in Cart"
                    btnText="Okay, Got it"
                />
            </View>
        )
    }
}
export default connect(state => ({}, mapDispatch))(CartItem);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}
const styles = StyleSheet.create({
    iconcart: {
        alignItems: 'flex-end',
        width: '20%',
        paddingRight: 15
    },
    firstCon: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 24,
        marginBottom: 10,
        paddingBottom: 10,
        backgroundColor: '#fff'
    },
    iconback: {
        width: '20%',
        paddingLeft: 15
    },
    textmain: {
        fontSize: 16,
        paddingTop: 5,
    },
    text: {
        width: '60%',
        alignItems: 'center',
    },
    main: {
        paddingTop: 30,
        paddingLeft: 10,
        paddingRight: 10
    },
    scroll: {
        //height: height - 150,
        // borderWidth:2,
        //flex:4
    },
    map: {
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,

    },
    image: {
        width: '100%',
        height: '100%'
    },
    btn: {
        alignItems: 'flex-end',
    },
    btnout: {
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 10,
    },
    btntext: {
        color: 'grey',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 6,
        paddingTop: 6,
    },
    imgcont: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '30%',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 10
    },
    imgout: {
        flexDirection: 'row',
    },
    imgmain: {
        height: 60,
        width: 60,
        overflow: 'hidden'
    },
    textout: {
        width: '45%',
        paddingLeft: 10,
        justifyContent: 'center'
    },
    textdetail: {
        fontSize: 15,
        marginBottom: 4
    },
    textdetails: {
        fontSize: 12,
        color: 'rgba(0,0,0,0.4)'
    },
    touchableOut: {
        width: '25%',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    priceOut: {
        flexDirection: 'row',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 20,
        paddingBottom: 30,
        backgroundColor: '#fff'
    },
    pricecont: {
        width: '50%'
    },
    pricetext: {
        fontSize: 24
    },
    pricetextout: {
        width: '50%',
        alignItems: 'flex-end'
    },
    btncheckout: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#fff'
    },
    map: {
        paddingLeft: 10,
        marginBottom: 10,
        paddingRight: 10,
        backgroundColor: '#fff'
    },
    imgout: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)'
    },
    imageoutlines: {
        height: 70,
        width: 70,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: '100%',
        height: '100%'
    },
})