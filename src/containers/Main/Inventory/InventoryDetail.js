import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Dimensions } from 'react-native'
import { Icon } from 'native-base'
import { Actions } from 'react-native-router-flux';
import Swiper from 'react-native-swiper';
import GradientButton from '../../../components/Buttons/GradientButton';
var { height, width } = Dimensions.get('window')

export default class InventoryDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}
	render() {
		return (
			<View style={styles.main}>
				<View style={styles.uppercon}>
					<View style={styles.firstCon}>
						<View style={styles.iconback}>
							<TouchableOpacity onPress={() => { Actions.pop() }}>
								<Icon name="ios-arrow-back" style={styles.icon} />
							</TouchableOpacity>
						</View>
						<View style={styles.text}>
							<Text style={styles.textmain}>Gold Armor</Text>
						</View>
						<TouchableOpacity style={styles.iconcart}>
							<Icon name="ios-cart-outline" />
						</TouchableOpacity>
					</View>
					<View style={styles.imgcon}>
						<View style={styles.contstart}>
						</View>
						<View style={styles.imageborder}>
							<View style={styles.imageOut}>
								<Swiper style={styles.wrapper} showsButtons={true} autoplayTimeout={2} showsButtons={false} dotColor='#fff'>
									<View>
										<Image style={styles.imgOut} source={require('../../../img/1.png')} />
									</View>
									<View>
										<Image style={styles.imgOut} source={require('../../../img/2.png')} />
									</View>
									<View>
										<Image style={styles.imgOut} source={require('../../../img/6.png')} />
									</View>
								</Swiper>
							</View>
						</View>
						<View style={styles.likeout}>
						</View>
					</View>
				</View>
				<View style={styles.lowercon}>
					<View style={styles.alltext}>
						<View style={styles.flexrow1}>
							<View style={styles.titleout}>
								<Text style={styles.title}>Gold Armor</Text>
							</View>
							<View style={styles.priceout}>
								<Text style={styles.price} >$ 999</Text>
							</View>
						</View>
						<View style={styles.flexrow2}>
							<View style={styles.gameout}>
								<Text style={styles.game} >Game: Witch </Text>
							</View>
							<View style={styles.rarityout}>
								<Text style={styles.raritygrey} >Rarity:</Text>
								<Text style={styles.raritypink} >Rare</Text>
							</View>
						</View>
						<View style={styles.flexrow}>
							<View>
								<Text style={styles.items} >Item type:Cloth</Text>
							</View>
						</View>
						<View style={styles.descout}>
							<Text style={styles.items}>
								The Desert Voe Set is composed of the Desert Voe Headband the Desert Voe Spaulder and the Desert Voe Trousers The full Desert Voe Set can be purchased for a total of 2,400 Rupees from either the Gerudo Secret Club in Gerudo Town or from Rhondson Armor Boutique in Tarrey Town.
						</Text>
						</View>
					</View>
					<View style={styles.btnview}>
						<View style={styles.btnout}>
							<View style={styles.width50}>
								<TouchableOpacity style={styles.editButtn}>
									<Text>Transfer</Text>
								</TouchableOpacity>
							</View>
							<View style={styles.width50}>
								<View style={styles.btnSell}>
									<GradientButton label="Sell" onPress={() => { Actions.SellItem() }} />
								</View>
							</View>
						</View>
					</View>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	uppercon: {
		flex: 1
	},
	lowercon: {
		flex: 1
	},
	iconcart: {
		alignItems: 'flex-end',
		width: '20%'
	},
	firstCon: {
		flexDirection: 'row',
		width: '100%'
	},
	iconback: {
		width: '20%',
	},
	textmain: {
		fontSize: 20,
		paddingTop: 5,
	},
	text: {
		width: '60%',
		alignItems: 'center',
	},
	main: {
		flex: 1,
		paddingTop: 20,
		paddingLeft: 10,
		paddingRight: 10,
		backgroundColor: '#fff'
	},
	imageborder: {
		width: '60%'
	},
	flexrow2: {
		flexDirection: 'row',
		paddingBottom: 3
	},
	flexrow1: {
		flexDirection: 'row',
		paddingBottom: 5
	},
	flexrow: {
		flexDirection: 'row',
		paddingBottom: 8
	},
	titleout: {
		width: '80%'
	},
	title: {
		fontSize: 20,
	},
	priceout: {
		width: '20%',
		alignItems: 'flex-end'
	},
	price: {
		fontSize: 20
	},
	gameout: {
		width: '50%',
		flexDirection: 'row'
	},
	game: {
		fontSize: 14,
		color: 'grey'
	},
	rarityout: {
		width: '50%',
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
	raritygrey: {
		fontSize: 14,
		color: 'grey'
	},
	raritypink: {
		fontSize: 14,
		color: 'violet'
	},
	items: {
		fontSize: 14,
		color: 'grey'
	},
	descout: {
		paddingTop: 8,
		borderTopWidth: 0.5,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	btnout: {
		paddingLeft: 10,
		paddingRight: 10,
		flexDirection: 'row'
	},
	btn: {
		borderRadius: 10,
		height: 50,
		backgroundColor: '#bdbfc1',
		justifyContent: 'center'
	},
	linear: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 50,
		borderRadius: 10
	},
	btntext: {
		color: 'white',
		fontSize: 18
	},
	likeout: {
		width: '20%',
		alignItems: 'center',
	},
	likeborder: {
		height: 55,
		width: 55,
		backgroundColor: 'white',
		borderRadius: 28,
		shadowColor: '#888888',
		shadowOpacity: 0.8,
		alignItems: 'center',
		justifyContent: 'center',
	},
	liketouch: {
		alignItems: 'center'
	},
	likeicon: {
		color: 'red'
	},
	like: {
		color: 'grey'
	},
	imgcon: {
		flexDirection: 'row',
		paddingTop: 10
	},
	contstart: {
		width: '20%'
	},
	btnview: {
		flex: 1,
		justifyContent: 'flex-end',
		paddingBottom: 10
	},
	alltext: {
		flex: 2,
		paddingTop: 10
	},
	width50: {
		width: '50%'
	},
	editButtn: {
		marginRight: 10,
		borderWidth: 1,
		borderRadius: 10,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center'
	},
	imgOut: {
		width: '100%',
		height: '100%'
	},
	btnSell: {
		marginLeft: 10
	},
	imageOut: {
		height: height / 2 - 60,
		justifyContent: 'center'
	}
});