import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, StatusBar } from 'react-native'
import GradientButton from '../../../components/Buttons/GradientButton';
import BadgeHeader from '../../../components/Navigation/BadgeHeader';
var { height, width } = Dimensions.get('window');
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import PopupComp from '../../../components/Popup/Popup';
import { checkout, accountDetails } from '../../../actions/MarketPlace';
class Payment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cartId: [],
            btnDisable: true,
            balance:''
        }
    }
    componentWillMount(){
        if (this.getTotalPrice() !== 0) {
            this.setState({ btnDisable: false })
        }
        this.props.dispatch(accountDetails()).then(res => {
            let { balance } = this.state;
            balance = res.balance;
            this.setState({ balance })
        })
    }
    getTotalPrice() {
        let totalPrice = 0
        this.props.CartData.cartItemList.map(item => { totalPrice = totalPrice + parseInt(item.price) })
        return totalPrice
    }
    popupOpen() {
        if (this.state.btnDisable === true) {
            this.popupErrorDialog.show()
        }else{
        this.props.dispatch(checkout()).then(res => {
        })
        this.popupDialog.show()
    }
    }
    popupClose() {
        // var tab = 2
        // Actions.Main({tab});
        Actions.Purchases()
        this.popupDialog.dismiss();
    }
    popupErrorClose() {
        this.popupErrorDialog.dismiss()
    }

    render() {
        let cartLength = this.props.CartData.cartItemList.length
        return (
            <View style={{flex:1}}>
                <StatusBar barStyle="dark-content" />
                <BadgeHeader lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Payment" badgeNo={cartLength} firstCon={styles.firstCon} />
                <View style={styles.main}>
                    <View style={styles.textout}>
                        <Text>My balance</Text>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.priceOut}>
                            <Text style={styles.priceText}>${this.state.balance}</Text>
                        </View>
                        <View style={styles.fundOut}>
                            <TouchableOpacity>
                                <Text style={styles.text}>Add fund</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View >
                        <View style={styles.totalOut}>
                            <Text style={styles.Total}>Total Price</Text>
                        </View>
                        <View style={styles.dollorOut}>
                            <Text style={styles.dollor}>$ {this.getTotalPrice()}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.btncheckout}>
                    <GradientButton label={"Pay $" + this.getTotalPrice()} onPress={this.popupOpen.bind(this)} />
                </View>
                <PopupComp link={(popupDialog) => { this.popupDialog = popupDialog; }}
                    onPress={this.popupClose.bind(this)} label='CONGRATULATION' description="Your items are sent to Purchase History"
                    btnText="Okay, Got it"
                />
                <PopupComp link={(popupErrorDialog) => { this.popupErrorDialog = popupErrorDialog; }}
                    onPress={this.popupErrorClose.bind(this)} label='Error' description="Please select items in Cart"
                    btnText="Okay, Got it"
                />
            </View>
        )
    }
}
export default connect(state => ({}, mapDispatch))(Payment);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}
const styles = StyleSheet.create({
    main: {
      //  height: height - 70,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        flex:6
    },
    textout: {
        paddingTop: 15
    },
    content: {
        flexDirection: 'row',
        paddingTop: 15,
        marginBottom: 40
    },
    text: {
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 5,
        fontSize: 12,
        paddingBottom: 5
    },
    btncheckout: {
        flex:1,
       // paddingTop: 10,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 10,
        width: '100%'
    },
    firstCon: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 34,
        backgroundColor: '#fff',
        marginBottom: 10,
        paddingBottom: 10
    },
    priceOut: {
        width: '50%'
    },
    priceText: {
        fontSize: 20
    },
    fundOut: {
        width: '50%', alignItems: 'flex-end',
    },
    Total: {
        fontSize: 15
    },
    totalOut: {
        alignItems: "center",
    },
    dollorOut: {
        alignItems: "center",
        paddingTop: 20
    },
    dollor: {
        fontSize: 40,
        color: 'blue'
    }
})