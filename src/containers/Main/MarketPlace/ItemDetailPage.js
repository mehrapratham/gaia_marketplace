import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Dimensions, StatusBar } from 'react-native'
import { Icon } from 'native-base'
import { Actions } from 'react-native-router-flux';
import GradientButton from '../../../components/Buttons/GradientButton'
import { itemDetail, CartItems } from '../../../actions/MarketPlace'
import { connect } from 'react-redux'
import BadgeHeaders from '../../../components/Navigation/BadgeHeader';
var { height, width } = Dimensions.get('window')
class ItemDetailPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			showlike: false,
			loader: false,
			carts: [],
		}
	}
	componentDidMount() {
		this.setState({ loader: true })
		this.props.dispatch(itemDetail(this.props.id)).then(res => {

		})
		this.props.dispatch(CartItems(123, 'show')).then(res => {
			this.setState({ loader: false })
		})
	}
	onClick(id) {
		this.setState({ loader: true })
		let data = this.props.MarketPlace && this.props.MarketPlace.itemDetailList || {}
		//this.props.dispatch(addToCart(data))
		this.setState({ toggleBtn: true })
		this.props.dispatch(CartItems(data.id, 'add')).then(res => {
			this.props.dispatch(CartItems(123, 'show')).then(res => {
				this.setState({ loader: false })
			})
		})
	}

	onlikeClick = (bool) => {
		this.setState({ showlike: bool })
	}
	render() {
		let itemDetail = this.props.MarketPlace && this.props.MarketPlace.itemDetailList || {}
		let cartList = this.props.CartData.cartItemList
		let itemArray = this.props.MarketPlace && this.props.MarketPlace.browseMarketList && this.props.MarketPlace.browseMarketList.item_list || {}
		let toggleBtn = cartList.findIndex(obj => obj.id === itemDetail.id)
		let newArray = itemArray.filter(item => item.id == itemDetail.id)
		return (
			<View style={styles.main}>
				<StatusBar barStyle="dark-content" />
				{this.state.loader ?
					<View style={styles.imageoutlines}>
						<Image source={require('../../../img/loader.gif')} style={styles.image} />
					</View> :
					<View>
						{newArray.map((val, key) => {
							return (
								<View key={key} style={styles.uppercon}>
									<View style={styles.uppercon}>
										<BadgeHeaders lefticon="ios-arrow-back" righticon='ios-cart-outline' onPress={() => { Actions.CartItem() }} label={val.name} badgeNo={cartList.length} firstCon={styles.firstCon} />
										<View style={styles.imgcon}>
											<View style={styles.contstart}>
											</View>
											<View style={styles.imageborder}>
												<View style={styles.imgOut}>
													<View style={styles.imgSize}>
														<Image style={styles.image} source={{ uri: val.thumbfile }} />
													</View>
												</View>
											</View>
											<View style={styles.likeout}>
												<View style={styles.likeborder}>
													{this.state.showlike ?
														<TouchableOpacity style={styles.liketouch} onPress={() => this.onlikeClick(false)}>
															<Icon name="ios-heart" style={styles.likeicon} />
															<Text>{val.likes}k</Text>
														</TouchableOpacity>
														:
														<TouchableOpacity style={styles.liketouch} onPress={() => this.onlikeClick(true)}>
															<Icon name="ios-heart-outline" style={styles.like} />
															<Text>{val.likes}k</Text>
														</TouchableOpacity>
													}
												</View>
											</View>
										</View>
									</View>
									<View style={styles.lowercon}>
										<View style={styles.alltext}>
											<View style={styles.flexrow1}>
												<View style={styles.titleout}>
													<Text style={styles.title}>{val.name}</Text>
												</View>
												<View style={styles.priceout}>
													<Text style={styles.price} >$ {val.price}</Text>
												</View>
											</View>
											<View style={styles.flexrow2}>
												<View style={styles.gameout}>
													<Text style={styles.game} numberOfLines={1}>Game: {val.name}</Text>
												</View>
												<View style={styles.rarityout}>
													<Text style={styles.raritygrey} >Rarity:</Text>
													<Text style={styles.raritypink} >{val.rarity}</Text>
												</View>
											</View>
											<View style={styles.flexrow}>
												<View>
													<Text style={styles.items} >Item type:Cloth</Text>
												</View>
											</View>
											<View style={styles.descout}>
												<Text style={styles.items}>
													{itemDetail.description}
												</Text>
											</View>
										</View>
										<View style={styles.btnview}>
											{toggleBtn != -1 ?
												<View style={styles.btnout}>
													<TouchableOpacity style={styles.btn} activeOpacity={1}>
														<Icon name='ios-checkmark' style={styles.iconCheck} />
													</TouchableOpacity>
												</View> :
												<View style={styles.btnout}>
													<GradientButton label="Add to Cart" onPress={this.onClick.bind(this)} />
												</View>
											}
										</View>
									</View>
								</View>
							)
						})}
					</View>
				}
			</View>
		);
	}
}
export default connect(state => ({}, mapDispatch))(ItemDetailPage);

const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}
const styles = StyleSheet.create({
	uppercon: {
		flex: 1
	},
	lowercon: {
		flex: 1,
		paddingLeft: 10,
		paddingRight: 10,
	},
	iconcart: {
		alignItems: 'flex-end',
		width: '20%'
	},
	// firstCon: {
	// 	flexDirection: 'row',
	// 	width: '100%',
	// 	paddingTop: 24
	// },
	iconback: {
		width: '20%',
	},
	textmain: {
		fontSize: 20,
		paddingTop: 5,
	},
	text: {
		width: '60%',
		alignItems: 'center',
	},
	main: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	imageborder: {
		width: '70%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	flexrow2: {
		flexDirection: 'row',
		paddingBottom: 3
	},
	flexrow1: {
		flexDirection: 'row',
		paddingBottom: 5
	},
	flexrow: {
		flexDirection: 'row',
		paddingBottom: 8
	},
	titleout: {
		width: '80%'
	},
	title: {
		fontSize: 18,
	},
	priceout: {
		width: '20%',
		alignItems: 'flex-end'
	},
	price: {
		fontSize: 18
	},
	gameout: {
		width: '70%',
		flexDirection: 'row'
	},
	game: {
		fontSize: 14,
		color: 'grey'
	},
	rarityout: {
		width: '30%',
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
	raritygrey: {
		fontSize: 14,
		color: 'grey'
	},
	raritypink: {
		fontSize: 14,
		color: 'violet'
	},
	items: {
		fontSize: 14,
		color: 'grey'
	},
	descout: {
		paddingTop: 8,
		borderTopWidth: 0.5,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	btnout: {
		paddingLeft: 20,
		paddingRight: 20
	},
	btn: {
		borderRadius: 10,
		height: 50,
		backgroundColor: '#bdbfc1',
		justifyContent: 'center'
	},
	linear: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		height: 50,
		borderRadius: 10
	},
	btntext: {
		color: 'white',
		fontSize: 18
	},
	likeout: {
		width: '20%',
		alignItems: 'center',
	},
	likeborder: {
		height: 55,
		width: 55,
		backgroundColor: 'white',
		borderRadius: 28,
		shadowColor: '#888888',
		shadowOpacity: 0.8,
		alignItems: 'center',
		justifyContent: 'center',
	},
	liketouch: {
		alignItems: 'center'
	},
	likeicon: {
		color: 'red'
	},
	like: {
		color: 'grey'
	},
	imgcon: {
		flexDirection: 'row',
		paddingTop: 10,
		paddingLeft: 10,
		paddingRight: 10,
	},
	contstart: {
		width: '10%'
	},
	btnview: {
		flex: 1,
		justifyContent: 'flex-end',
		paddingBottom: 20
	},
	alltext: {
		flex: 2,
		paddingTop: 10
	},
	imageoutlines: {
		height: 70,
		width: 70,
		overflow: 'hidden',
		alignItems: 'center',
		justifyContent: 'center'
	},
	image: {
		width: '100%',
		height: '100%'
	},
	iconCheck: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 36
	},
	imgOut: {
		height: height / 2 - 60,
		justifyContent: 'center',
		alignItems: 'center'
	},
	imgSize: {
		height: 100,
		width: 100
	},
	firstCon: {
		flexDirection: 'row',
		width: '100%',
		paddingTop: 34,
	},
});