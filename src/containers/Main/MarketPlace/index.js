import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, StatusBar, TouchableWithoutFeedback, ScrollView, Image, Dimensions, Keyboard } from 'react-native'
import { Icon, Container, Content } from 'native-base'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import GameCards from '../../../components/Cards/GameCards';
import Autocomplete from 'react-native-autocomplete-input'
import { categoryList, initialLoad } from '../../../actions/MarketPlace'
var { height, width } = Dimensions.get('window');
class MarketPlace extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			query: '',
			loader: false,
			activeTabs: 0,
			hideResults: false,
			games: [
				{
					title: 'abc'
				},
				{
					title: 'xyz'
				},
				{
					title: 'abdjkheufyg'
				},
				{
					title: 'abdjkheufyg'
				},
				{
					title: 'abdjkheufyg'
				},
				{
					title: 'abdjkheufyg'
				},
				{
					title: 'abdjkheufyg'
				},
				{
					title: 'abvdvc'
				},
				{
					title: 'hllo'
				},
				{
					title: 'www'
				},
				{
					title: 'qqq'
				},
			],
			item: [
				{
					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{

					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{
					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{

					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{

					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{

					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{

					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				},
				{
					name: 'Heart Container',
					image: '../../img/game.png',
					dollar: '738',
					like: '9k'
				}
			]
		}
	}
	onChange(tabs) {
		const { activeTabs } = this.state;
		this.setState({ activeTabs: tabs })
	}
	findgame(query) {
		if (query === '') {
			return [];
		}
		const { games } = this.state;
		const regex = new RegExp(`${query.trim()}`, 'i');
		const searchedGames = games.filter(game => game.title.search(regex) >= 0);
		return searchedGames.length ? searchedGames : [{
			title: 'No results found',
			notClickable: true
		}]
	}
	componentWillMount() {
		this.props.dispatch(initialLoad())
		this.setState({ loader: true })
		this.props.dispatch(categoryList()).then(res => {
			this.setState({ loader: false })
		})
	}
	goToIconDetailPage(obj) {
		Actions.IconDetailPage({ obj })
	}
	gamelist(item) {
		let gameArray = []
		for (var key in item) {
			gameArray.push(item[key]);
		}
		return gameArray
	}
	onPressInput() {
		Actions.SearchDetails()
		Keyboard.dismiss()
		this.setState({ hideResults: true })
		this.setState({ query: '' })
	}
	onTextChange(text) {
		this.setState({ query: text })
	}
	onHideResults() {
		this.setState({ hideResults: true })
		this.setState({ query: '' })
		Keyboard.dismiss()
	}
	getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}

	render() {
		var { height, width } = Dimensions.get('window');
		const { query } = this.state;
		const games = this.findgame(query);
		const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
		const catList = this.props.MarketPlace && this.props.MarketPlace.categoryList || [];
		const searchResults = games.length === 1 && comp(query, games[0].title) ? [] : games
		return (
			<Container style={styles.mainCon}>
				{this.state.loader ?
					<View style={styles.loaderOut}>
						<View style={styles.imageoutlines}>
							<Image resizeMode='contain' source={require('../../../img/loader.gif')} style={styles.image} />
						</View>
					</View> :
					<Content accessible={false} activeOpacity={1} style={styles.container}>
					<StatusBar barStyle='light-content'/>
						<View style={styles.relativeView}>
							<View style={styles.secondcon}>
								<View style={styles.backCon}>
									<Image resizeMode="stretch" style={styles.image} source={require('../../../img/img.png')} />
								</View>
								<View style={styles.relativeView2}>
									<View style={styles.searchback}>
										<View style={styles.gameTitleSearch}>
											<Icon name="search" style={styles.iconStyle} />
											<Autocomplete
												inputContainerStyle={styles.inputContainerStyle}
												containerStyle={styles.containerStyle}
												listStyle={styles.lifestyle}
												hideResults={this.state.hideResults}
												listContainerStyle={styles.listContainerStyle}
												autoCapitalize="none"
												onBlur={this.onHideResults.bind(this)}
												onFocus={() => { this.setState({ hideResults: false }) }}
												underlineColorAndroid='rgba(0,0,0,0)'
												autoCorrect={false}
												data={searchResults}
												defaultValue={query}
												onChangeText={this.onTextChange.bind(this)}
												placeholder="Enter Game title"
												renderItem={({ title, notClickable = false }) => (
													<TouchableOpacity onPress={notClickable ? () => { } : this.onPressInput.bind(this)}
														style={styles.touchableOut}>
														<Text style={styles.touchableText}>
															{title}
														</Text>
													</TouchableOpacity>
												)}
											/>
										</View>
										<View style={styles.itemoutline}>
											<Icon name="ios-cart-outline" style={styles.gameTitleIcon} />
										</View>
									</View>
									<View style={styles.mainlist}>
										<ScrollView horizontal={true} style={styles.listscroll} showsHorizontalScrollIndicator={false}>
											<TouchableOpacity style={this.state.activeTabs == 0 ? styles.listitemfirst : styles.padding10} onPress={this.onChange.bind(this, 0)}>
												<Text style={styles.listtext}>All Items</Text>
											</TouchableOpacity>
											{
												catList.map((item, key) => {
													return (
														<TouchableOpacity key={key} style={this.state.activeTabs == item.id ? styles.listitemfirst : styles.padding10} onPress={this.onChange.bind(this, item.id)}>
															<Text style={styles.listtext}>{item.category_name}</Text>
														</TouchableOpacity>
													)
												})
											}
										</ScrollView>
									</View>
									<View style={styles.titleOut}>
										<Text style={styles.titletext}>Gaia Games</Text>
									</View>
									<View style={styles.imgOut}>
										<ScrollView horizontal={true} style={[styles.padding5, { flexDirection: 'row' }]} showsHorizontalScrollIndicator={false}>
											{
												catList.map((item, key) => {
													return (
														<View  key={key} style={[styles.imgcon]}>
															<Image resizeMode="stretch" source={{ uri: item.thumbfile }} style={styles.img} />
														</View>
													)
												})
											}
										</ScrollView>
									</View>
									{
										catList.map((item, key) => {
											return (
												<View key={key}>
													{this.state.activeTabs == 0 || this.state.activeTabs == item.id ?
														<View>
															<View style={styles.itemscon} >
																<View style={styles.itemconwidth}>
																	<Text style={styles.itemtext}>{item.category_name}</Text>
																</View>
																<TouchableOpacity style={styles.moreitem} onPress={this.goToIconDetailPage.bind(this, item)}>
																	<View style={styles.lineOut}>
																		<Text style={styles.moreitemtext}>See All</Text>
																		<Icon style={styles.moreitemicon} name="ios-arrow-forward" />
																	</View>
																</TouchableOpacity>
															</View>
															<View style={styles.padding101}>
																<ScrollView horizontal={true} style={styles.padding5} showsHorizontalScrollIndicator={false}>
																	<GameCards list={this.gamelist(item.games_list)} />
																</ScrollView>
															</View>
														</View>
														: null}
												</View>
											)
										})
									}
								</View>
							</View>
						</View>
					</Content>
				}
			</Container>
		)
	}
}
export default connect(state => ({}, mapDispatch))(MarketPlace);

const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}

/*MarketPlace page css as json*/
const styles = StyleSheet.create({
	mainCon: {
		flex: 1,
		justifyContent: 'center',
	},
	gameTitleIcon: {
		color: '#fff'
	},
	loaderOut: {
		height: height
	},
	focusedTextInput: {
		height: 30,
		paddingLeft: 10,
		paddingRight: 30,
		width: '100%'
	},
	gameTitleSearch: {
		flexDirection: 'row',
		width: '85%',
	},
	iconStyle: {
		fontSize: 20,
		marginTop: 12,
		marginLeft: 7,
		color: '#6580ea'
	},
	container: {
		flex: 1
	},
	secondcon: {
		paddingTop: 34
	},
	searchback: {
		flexDirection: 'row',
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 5,
	},
	itemoutline: {
		width: '15%',
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		paddingTop: 5
	},
	mainlist: {
		paddingLeft: 10,
		paddingRight: 10,
		position: 'relative',
		zIndex: -1,
		paddingTop:15
	},
	listscroll: {
		paddingBottom: 5
	},
	listitemfirst: {
		padding: 10,
		backgroundColor: 'rgba(0,0,0,0.1)',
		borderRadius: 50,
		zIndex: 0.1
	},
	listtext: {
		color: '#fff',
		fontSize: 14
	},
	padding10: {
		padding: 10,
	},
	titleOut: {
		padding: 10,
		zIndex: -1
	},
	padding5: {
		paddingBottom: 5,
	},
	titletext: {
		color: '#fff',
		fontSize: 28,
		fontWeight: '500',
	},
	imgcon: {
		height: 150,
		width: 150,
		borderRadius: 10,
		// overflow: 'hidden',
		marginRight: 10,
	},
	img: {
		width: '100%',
		height: '100%',
		overflow: 'hidden',
		borderRadius: 10
	},
	itemscon: {
		flexDirection: 'row',
		padding: 10
	},
	itemconwidth: {
		width: '70%'
	},
	itemtext: {
		fontSize: 20
	},
	moreitem: {
		alignItems: 'flex-end',
		width: '30%'
	},
	moreitemtext: {
		color: '#6580ea',
		fontSize: 16,
		paddingRight: 8
	},
	moreitemicon: {
		paddingTop: 2,
		fontSize: 16,
		color: '#6580ea'
	},
	backCon: {
		height: 300,
		position: 'absolute',
		zIndex: 0.3,
		width: '100%',
		justifyContent: 'flex-end',
		overflow: 'hidden'
	},
	relativeView2: {
		position: 'relative',
		zIndex: 1,
	},
	relativeView: {
		position: 'relative'
	},
	backConText: {
		display: 'none'
	},
	autocompleteContainer: {

	},
	itemText2: {

	},
	textbox: {

	},
	inputContainerStyle: {
		borderRadius: 8,
		overflow: 'hidden',
		borderColor: 'transparent'
	},
	touchableOut: {
		borderColor: '#c6c6c6',
		borderWidth: 0.7
	},
	touchableText: {
		fontSize: 17,
		margin: 2,
		padding: 5
	},
	containerStyle: {
		width: '100%',
		position: 'absolute',
		zIndex: 1,
	},
	lifestyle: {
		width: '100%',
		zIndex: 1
	},
	listContainerStyle: {
		padding: 1,
		zIndex: 1,
		position: 'relative'
	},
	padding101: {
		paddingLeft: 10,
		paddingTop: 10
	},
	imgOut: {
		paddingLeft: 10,
		paddingTop: 10,
		zIndex: -1,
		backgroundColor: 'transparent'
	},

	imageoutlines: {
		height: 70,
		width: 70,
		marginTop: height / 2 - 50,
		overflow: 'hidden',
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: width / 2 - 35,
	},
	image: {
		width: '100%',
		height: '100%'
	},
	lineOut: {
		flexDirection: "row"
	}
})