import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, Dimensions, StatusBar, SafeAreaView } from 'react-native'
import { Icon, Drawer } from 'native-base'
import { Actions } from 'react-native-router-flux';
import MenuList from '../../../components/Lists/MenuList'
import { connect } from 'react-redux'
import { browseMarket } from '../../../actions/MarketPlace'
var { height, width } = Dimensions.get('window');
class IconDetailPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpened: false,
			loader: false
		}
	}
	componentWillMount() {
		let detailList = this.props.obj
		this.setState({ loader: true })
		this.props.dispatch(browseMarket(detailList.id)).then(res => {
			this.setState({ loader: false })
		})
	}
	closeDrawer = () => {
		this.drawer._root.close()
		this.setState({ isOpened: false })
	};
	openDrawer() {
		if (this.state.isOpened) {
			this.closeDrawer()
		} else {
			this.drawer._root.open()
			this.setState({ isOpened: true })
		}
	};
	goToItemDetailPage(id) {
		Actions.ItemDetailPage({ id })
	}
	getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	render() {
		var { height, width } = Dimensions.get('window');
		let itemList = this.props.MarketPlace && this.props.MarketPlace.browseMarketList && this.props.MarketPlace.browseMarketList.item_list || []
		const catList = this.props.MarketPlace && this.props.MarketPlace.categoryList || [];
		let detailList = this.props.obj;
		return (
			<View style={styles.flexOne}>
				<StatusBar barStyle="dark-content" />
				{this.state.loader ?
					<View style={styles.imageoutlines}>
						<Image source={require('../../../img/loader.gif')} style={styles.image} />
					</View> :
					<Drawer
						ref={(ref) => { this.drawer = ref; }}
						side="right"
						content={<View style={{ backgroundColor: '#fff', height: '100%' }}>
							<MenuList onPressDone={this.closeDrawer.bind(this)} />
						</View>}
						onClose={() => this.closeDrawer()}
						tapToClose={true}
						style={styles.flexOnes}
						openDrawerOffset={0.2}>
						<View style={styles.flex1}>
							<SafeAreaView style={styles.safeCon} >
								<View style={styles.firstCon}>
									<View style={styles.secondcon}>
										<TouchableOpacity onPress={() => { Actions.pop() }}>
											<Icon name="ios-arrow-back" style={styles.icon} />
										</TouchableOpacity>
										<Text style={styles.textmain}>{detailList.category_name}</Text>
									</View>
									<View style={styles.iconcart}>
										<Icon name="ios-cart-outline" style={styles.icons} />
									</View>
								</View>
							</SafeAreaView>
							<ScrollView style={styles.scroll}>
								<View style={styles.map}>
									<View style={styles.drawerOut}>
										<View style={styles.width50}>
											<Text style={styles.textResults}>{itemList.length} results</Text>
										</View>
										<View style={styles.width50}>
											<TouchableOpacity onPress={this.openDrawer.bind(this)}><Text style={styles.textDrawer}>SORT AND FILTER</Text></TouchableOpacity>
										</View>
									</View>
									{itemList.map((item, key) => {
										return (
											<TouchableOpacity style={styles.container} key={key} onPress={this.goToItemDetailPage.bind(this, item.id)}>
												<View style={[styles.imgOut, { backgroundColor: this.getRandomColor() }]}>
													<View style={styles.imageoutline}>
														<Image source={{ uri: item.thumbfile }} style={styles.image} />
													</View>
												</View>
												<View style={styles.namecon}>
													<View style={styles.nameinnercon}>
														<Text style={styles.itemname} numberOfLines={1}>{item.name}</Text>
													</View>
													<View style={styles.pricecon}>
														<View style={styles.priceinnercon}>
															<Text style={styles.price}>$ {item.price}</Text>
														</View>
														<View style={styles.likecon}>
															<Text>
																<Text style={styles.like}>{item.likes + ' '}</Text>
																<Icon name="ios-heart" style={styles.iconHeart} />
															</Text>
														</View>
													</View>
												</View>
											</TouchableOpacity>
										)
									})}
								</View>
							</ScrollView>
						</View>
					</Drawer>}
			</View>
		)
	}
}
export default connect(state => ({}, mapDispatch))(IconDetailPage);

const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}
const styles = StyleSheet.create({
	firstCon: {
		paddingLeft: 10,
		paddingRight: 10,
		backgroundColor: '#fff',
		flexDirection: 'row',
		paddingBottom: 5,
		marginBottom: 10,
		paddingTop:5
	},
	safeCon: {
		backgroundColor: '#fff',
		marginBottom: 10,
		paddingTop: 34,

	},
	secondcon: {
		width: '50%',
		flexDirection: 'row'
	},
	icon: {
		marginRight: 20,
		fontSize: 24
	},
	textmain: {
		fontSize: 16,
		paddingTop: 2
	},
	icons: {
		fontSize: 24
	},
	iconcart: {
		width: '50%',
		alignItems: 'flex-end'
	},
	scroll: {
	},
	map: {
		flexDirection: "row",
		flexWrap: 'wrap',
		paddingLeft: 10,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center'
	},
	container: {
		marginRight: 10,
		width: width / 2 - 15,
		marginBottom: 10,
		shadowColor: 'grey',
		shadowOffset: { width: 0, height: 5 },
		shadowOpacity: 0.8,
		shadowRadius: 5,
		elevation: 1,
	},
	imageoutline: {
		height: 70,
		width: 70,
		overflow: 'hidden'
	},
	imageoutlines: {
		height: 70,
		width: 70,
		overflow: 'hidden',
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 50,
		marginLeft: width / 2 - 35
	},
	image: {
		width: '100%',
		height: '100%'
	},
	namecon: {
		padding: 10,
		backgroundColor: '#fff',
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10,
		borderLeftWidth: 0.5,
		borderRightWidth: 0.5,
		borderBottomWidth: 0.5,
		borderColor: 'rgba(0,0,0,0.1)'
	},
	nameinnercon: {
		marginBottom: 5
	},
	itemname: {
		color: 'rgba(0,0,0,0.3)',
		fontSize: 12
	},
	pricecon: {
		flexDirection: 'row'
	},
	priceinnercon: {
		width: '50%'
	},
	price: {
		color: 'rgba(0,0,0,0.3)',
		fontSize: 12
	},
	likecon: {
		width: '50%',

		alignItems: 'flex-end'

	},
	like: {
		color: 'rgba(0,0,0,0.3)',
		fontSize: 12,
	},
	iconHeart: {
		fontSize: 12,
		color: 'rgba(0,0,0,0.3)',
	},
	flex1: {
		flex: 1
	},
	flexOne: {
		flex: 1,
		justifyContent: 'center',
	},
	flexOnes: {
		flex: 1
	},
	drawerOut: {
		width: '100%',
		flexDirection: 'row',
		paddingTop: 20,
		paddingBottom: 10,
		paddingBottom: 15
	},
	width50: {
		width: '50%'
	},
	textResults: {
		color: 'rgba(0,0,0,0.3)'
	},
	textDrawer: {
		color: '#6580ea',
		alignSelf: 'flex-end',
		marginRight: 10
	},
	imgOut: {
		paddingTop: 30,
		paddingBottom: 30,
		alignItems: 'center',
		justifyContent: 'center',
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10
	}
})