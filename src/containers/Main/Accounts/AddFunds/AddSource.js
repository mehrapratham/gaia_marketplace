import React from 'react'
import { View, TouchableOpacity, StyleSheet, Image, Dimensions, StatusBar } from 'react-native'
import Headers from '../../../../components/Navigation/Headers';
var { height, width } = Dimensions.get('window');
export default class AddSource extends React.Component {
    constructor() {
        super()
        this.state = {
            item: [
                {
                    image: require('../../../../img/p.png')
                },
                {
                    image: require('../../../../img/paypal.png')
                },
                {
                    image: require('../../../../img/applepay.png')
                },
                {
                    image: require('../../../../img/debit.png')
                },
            ]
        }
    }
    render() {
        return (
            <View>
                <StatusBar barStyle="dark-content" />
                <Headers lefticon="ios-arrow-back" righticon='ios-cart' label="Add Source" />
                <View style={styles.map}>
                    {this.state.item.map((item, key) => {
                        return (
                            <TouchableOpacity style={styles.container} key={key} >
                                <View style={styles.mainCon}>
                                    <View style={styles.imageoutline}>
                                        <Image resizeMode='contain' source={item.image} style={styles.image} />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        marginRight: 10,
        width: width / 2 - 15,
        marginBottom: 10,
        shadowColor: 'grey',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 8,
    },
    imageoutline: {
        height: 80,
        width: 80,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    map: {
        flexDirection: "row",
        flexWrap: 'wrap',
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainCon: {
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
    }
})