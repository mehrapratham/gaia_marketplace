import React from 'react'
import { View, Text, TouchableOpacity, Keyboard, StyleSheet, Dimensions, StatusBar,Linking , SafeAreaView } from 'react-native'
import GradientButton from '../../../../components/Buttons/GradientButton';
import Headers from '../../../../components/Navigation/Headers';
import TextField from '../../../../components/TextField';
import { Actions } from 'react-native-router-flux';
import { addBalance } from '../../../../actions/MarketPlace';
import { connect } from 'react-redux'
import PopupComp from '../../../../components/Popup/Popup';
var { height, width } = Dimensions.get('window');
import PayPal from 'react-native-paypal-integration';


class AddFunds extends React.Component {
    constructor() {
        super();
        this.state = {
            activeTabs: 0,
            price: '',
            bottom: false,
            keyHeight: 0,
            message: ''
        }
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    onChange(tabs, value) {
        const { activeTabs } = this.state;
        this.setState({ activeTabs: tabs, price: value })
    }
    onChangeText = (e) => {
        this.setState({ price: e })
    }
    _keyboardDidShow = (e) => {
        var keyboardHeight = e.endCoordinates.height
        this.setState({ keyHeight: keyboardHeight })
        this.setState({ bottom: true })
    }
    _keyboardDidHide = () => {
        this.setState({ bottom: false })
    }
    goToNextPage() {
        // this.setState({ bottom: false })
        // Keyboard.dismiss()
        // console.log(this.state.price, "00000")
        // this.props.dispatch(addBalance(this.state.price)).then(res => {
        //     this.setState({ message: res.message, price: '', activeTabs: 0 })
        //     this.popupDialog.show();
        // })
        //Actions.AddSource()
        PayPal.profileSharing({
            clientId: 'Your client ID',
            environment: PayPal.SANDBOX,
            merchantName: 'Your merchant name',
            merchantPrivacyPolicyUri: 'http://your-url.com/policy',
            merchantUserAgreementUri: 'http://your-url.com/legal',
            scopes: [
                PayPal.SCOPE_PROFILE, // Full Name, Birth Date, Time Zone, Locale, Language
                PayPal.SCOPE_PAYPAL_ATTRIBUTES, // Age Range, Account Status, Account Type, Account Creation Date
                PayPal.SCOPE_EMAIL, // Email
                PayPal.SCOPE_ADDRESS, // Address
                PayPal.SCOPE_PHONE // Telephone
            ]
          }, 
          function (r) {
            console.log(r);
          }
        );
    }
    popupClose() {
        this.popupDialog.dismiss()
        Actions.Accounts()
    }
    render() {
        var keyboardHeight = this.state.keyHeight
        return (
            <TouchableOpacity onPress={Keyboard.dismiss} accessible={false} activeOpacity={1} style={{ flex: 1 }}>
                <StatusBar barStyle="dark-content" />
                <Headers lefticon="ios-arrow-back" righticon='ios-cart' label="Add Funds" />
                <View style={styles.main}>
                    <View style={styles.textout}>
                        <Text>Sell Gold Armor for</Text>
                    </View>
                    <View style={styles.secondCon}>
                        <View style={styles.textPriceOut}>
                            <Text style={styles.textPrice}>Current Price: $2,138</Text>
                        </View>
                        <View style={styles.priceContainer}>
                            <Text style={styles.dollor}>$</Text>
                            <TextField onChange={this.onChangeText} placeholder="2,222" value={`${this.state.price}`} focusedOutTextInput={styles.priceInput} focusedTextInput={styles.priceInput} keyboardType='numeric' />
                            <Text style={styles.ussd}>ussd</Text>
                        </View>
                        <View style={styles.containerValue}>
                            <TouchableOpacity style={this.state.activeTabs == 1 ? styles.activeButton : styles.unactive} onPress={this.onChange.bind(this, 1, 5)}>
                                <Text style={this.state.activeTabs == 1 ? styles.activeText : styles.unactiveText}>$5</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={this.state.activeTabs == 2 ? styles.activeButton : styles.unactive} onPress={this.onChange.bind(this, 2, 10)}>
                                <Text style={this.state.activeTabs == 2 ? styles.activeText : styles.unactiveText}>$10</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={this.state.activeTabs == 3 ? styles.activeButton : styles.unactive} onPress={this.onChange.bind(this, 3, 25)}>
                                <Text style={this.state.activeTabs == 3 ? styles.activeText : styles.unactiveText}>$25</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ alignItems: 'center' }} style={this.state.activeTabs == 4 ? styles.activeButton2 : styles.unactive2} onPress={this.onChange.bind(this, 4, 50)}>
                                <Text style={this.state.activeTabs == 4 ? styles.activeText : styles.unactiveText} >$50</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={this.state.bottom ? [styles.btncheckout2, { marginBottom: keyboardHeight-20 }] : styles.btncheckout}>
                    <GradientButton label="Deposit" onPress={this.goToNextPage.bind(this)} />
                </View>
                <PopupComp link={(popupDialog) => { this.popupDialog = popupDialog; }}
                    onPress={this.popupClose.bind(this)} label='CONGRATULATION' description={this.state.message}
                    btnText="Okay, Got it"
                />
            </TouchableOpacity>
        )
    }
}

export default connect(state => ({}, mapDispatch))(AddFunds);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}

const styles = StyleSheet.create({
    main: {
        flex: 3,
        height: height - 70,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10
    },
    textout: {
        paddingTop: 15
    },
    content: {
        paddingTop: 15,
        marginBottom: 40
    },
    activeText: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
        fontSize: 12,
        paddingBottom: 5,
        color: 'white'
    },
    unactiveText: {
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
        fontSize: 12,
        paddingBottom: 5,

    },
    btncheckout: {
        // paddingTop: 10,
        justifyContent: 'flex-end',
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        flex: 1,
        backgroundColor: '#fff',
        // position: 'absolute',
        // bottom: 20,
        width: '100%'
    },
    btncheckout2: {
        paddingTop: 10,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 20,
        width: '100%',
    },
    firstCon: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 24,
        backgroundColor: '#fff',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 10,
        paddingBottom: 10
    },
    activeButton: {
        backgroundColor: "#6580ea",
        alignItems: 'center',
        marginRight: 20,
        borderRadius: 10,
    },
    unactive: {
        alignItems: 'center',
        marginRight: 20
    },
    activeButton2: {
        backgroundColor: "#6580ea",
        alignItems: 'center',
        borderRadius: 10,
    },
    unactive2: {
        alignItems: 'center',
    },
    secondCon: {
        marginTop: 30
    },
    textPriceOut: {
        alignItems: "center"
    },
    textPrice: {
        fontSize: 12,
        color: 'grey'
    },
    dollor: {
        fontSize: 15,
        paddingRight: 10
    },
    priceInput: {
        fontSize: 40,
        maxWidth: '70%'
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    ussd: {
        fontSize: 15,
        paddingLeft: 10
    },
    containerValue: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 20
    },
})