import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, StatusBar, Image } from 'react-native'
import { Icon, Container, Content, Header } from 'native-base'
import { LinearGradient } from 'expo';
import { Actions } from 'react-native-router-flux';
import { accountDetails } from '../../../actions/MarketPlace';
import { connect } from 'react-redux'


var { height, width } = Dimensions.get('window');
class Accounts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            balance: '',
            loader: false,
        }
    }
    componentWillMount() {
        //this.setState({ loader: true })
        this.props.dispatch(accountDetails()).then(res => {
            let { balance } = this.state;
            balance = res.balance;
            this.setState({ balance })
            this.setState({ loader: false })
        })
    }
    render() {
        return (
            <Container style={styles.maincon}>
                {this.state.loader ?
                    <View style={styles.loaderOut}>
                        <View style={styles.imageoutlines}>
                            <Image resizeMode='contain' source={require('../../../img/loader.gif')} style={styles.image} />
                        </View>
                    </View> :
                    <Content style={{ flex: 1 }}>
                        <Header style={{ height: 300, marginTop: -25, paddingTop: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0 }}>
                            <LinearGradient
                                colors={['#66e8dc', '#6580ea']}
                                start={[0.1, 0.1]} end={[1, 0.3]}
                                style={{ height: 300 }}>
                                <View style={styles.uppercon}>
                                    <View style={styles.firstCon}>
                                        <View style={{ width: '20%' }}>
                                        </View>
                                        <View style={styles.text}>
                                            <Text style={styles.textmain}>Username</Text>
                                        </View>
                                        <View style={styles.iconcart}>
                                            <Icon name="ios-cart-outline" style={styles.iconcart2} />
                                        </View>
                                    </View>
                                    <View style={styles.headingout}>
                                        <Text style={styles.headingtext}>My Balance</Text>
                                    </View>
                                    <View style={{ height: 180, justifyContent: 'center' }}>
                                        <View style={styles.priceout}>
                                            <Text style={styles.price}><Text style={{ fontSize: 18 }}>$</Text>{this.state.balance} <Text style={{ fontSize: 18 }}>USD</Text></Text>
                                        </View>
                                        <View style={styles.btn} >
                                            <TouchableOpacity style={styles.btnout} onPress={() => { Actions.AddFunds() }}>
                                                <Text style={styles.btntext}>Add Funds</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.btnout} onPress={() => { Actions.RemoveFunds() }}>
                                                <Text style={styles.btntext}>Remove Funds</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </LinearGradient>
                        </Header>
                        <View style={styles.lowercon}>
                            <StatusBar barStyle="light-content" />
                            <TouchableOpacity style={styles.touchableout} onPress={() => { Actions.Profile() }}>
                                <View style={styles.titleout}>
                                    <Text style={styles.titletext}> Profile </Text>
                                </View>
                                <View style={styles.iconout}>
                                    <Icon style={styles.iconforword} name="ios-arrow-forward" />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.touchableout} onPress={() => { Actions.Purchases() }}>
                                <View style={styles.titleout}>
                                    <Text style={styles.titletext} > Purchases </Text>
                                </View>
                                <View style={styles.iconout}>
                                    <Icon style={styles.iconforword} name="ios-arrow-forward" />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.touchableout} onPress={() => { Actions.Selling() }}>
                                <View style={styles.titleout}>
                                    <Text style={styles.titletext}> Selling </Text>
                                </View>
                                <View style={styles.iconout}>
                                    <Icon style={styles.iconforword} name="ios-arrow-forward" />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </Content>
                }
            </Container>

        );
    }
}
export default connect(state => ({}, mapDispatch))(Accounts);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}

const styles = StyleSheet.create({
    loaderOut: {
		height: height
	},
    iconcart: {
        alignItems: 'flex-end',
        width: '20%',
        marginTop: 5
    },
    iconcart2: {
        color: 'white',
        fontSize: 24
    },
    firstCon: {
        flexDirection: 'row',
        paddingTop: 52,
    },
    textmain: {
        fontSize: 18,
        paddingTop: 5,
        color: 'white'
    },
    text: {
        alignItems: 'center',
        width: '60%',
    },
    maincon: {
        flex: 1,
    },
    uppercon: {
        //  position: 'relative',
        paddingLeft: 10,
        paddingRight: 10,
        //paddingTop: 24,
    },
    liner: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        flex: 2,
        //height: height / 2 - 25,
    },
    titlebar: {
    },
    headingout: {
        paddingTop: 20,
        paddingLeft: 10
    },
    image: {
		width: '100%',
		height: '100%'
	},
    headingtext: {
        fontSize: 18,
        color: 'white'
    },
    priceout: {
        alignItems: 'center',
        width: '100%',

    },
    price: {
        fontSize: 35,
        color: 'white'
    },
    btn: {
        alignItems: 'center',
        paddingTop: 20,
    },
    btnout: {
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 10,
        width: 'auto',
        marginBottom: 10,
        height:30,
        width:130,
        justifyContent:'center',
        alignItems:'center'
    },
    btntext: {
        color: 'white',
    },
    lowercon: {
        flex: 1,
    },
    touchableout: {
        paddingTop: 20,
        paddingBottom: 20,
        flexDirection: 'row',
        borderBottomColor: '#d6d6d6',
        borderBottomWidth: 0.5
    },
    titleout: {
        alignItems: 'flex-start',
        width: '80%',
        paddingLeft: 20
    },
    titletext: {
        fontSize: 16
    },
    iconout: {
        alignItems: 'flex-end',
        width: '20%',
        paddingRight: 20
    },
    iconforword: {
        fontSize: 18,
        alignSelf: 'flex-end'
    },
    imageoutlines: {
		height: 70,
		width: 70,
		marginTop: height / 2 - 50,
		overflow: 'hidden',
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: width / 2 - 35,
	},
})