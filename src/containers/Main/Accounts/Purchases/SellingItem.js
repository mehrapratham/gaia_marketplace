import React from 'react'
import { View, Text, TouchableOpacity, Keyboard, StyleSheet, Dimensions } from 'react-native'
import Headers from '../../../../components/Navigation/Headers';
import { connect } from 'react-redux'
import GradientButton from '../../../../components/Buttons/GradientButton';
import TextField from '../../../../components/TextField';
import PopupComp from '../../../../components/Popup/Popup';
import { sellings } from '../../../../actions/MarketPlace';
import Toast from 'react-native-easy-toast';
import { Actions } from 'react-native-router-flux';
var { height, width } = Dimensions.get('window');
class SellingItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            bottom: false,
            price: ''
        }
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _keyboardDidShow = (e) => {
        var keyboardHeight = e.endCoordinates.height
        this.setState({ keyHeight: keyboardHeight })
        this.setState({ bottom: true })
    }
    _keyboardDidHide = () => {
        this.setState({ bottom: false })
    }
    popupOpen() {
        var items = this.props.data
        var price = this.state.price
        this.props.dispatch(sellings(items.serial, items.price, items.start_time, items.end_time, 1)).then(res => {
        })
        Keyboard.dismiss()
        this.setState({ bottom: false })
        this.popupDialog.show();
    }
    popupClose() {
        this.popupDialog.dismiss()
        Actions.Purchases()
    }
    onChangeText = (e) => {
        const price = this.state.price
        //this.setState({ price: e })
        let newText = '';
        let numbers = '0123456789';
        for (var i = 0; i < e.length; i++) {
            if (numbers.indexOf(e[i]) > -1) {
                newText = newText + e[i];
                this.setState({ price: e })
            }
            else {
                alert('please enter numbers only')

            }
        }
    }
    setPrice(val) {
        var price = this.state
        this.setState({ price: val })
    }
    render() {
        var keyboardHeight = this.state.keyHeight
        return (
            <TouchableOpacity onPress={Keyboard.dismiss} accessible={false} activeOpacity={1}>
                <Headers lefticon="ios-arrow-back" righticon='ios-cart' label="Sell" />
                <View style={styles.main}>
                    <View style={styles.textout}>
                        <Text>Sell Gold Armor for</Text>
                    </View>
                    <View style={styles.mainCon}>
                        <View style={styles.textSellOut}>
                            <Text style={styles.textPrice}>Current Price: ${this.props.data.price} </Text>
                        </View>
                        <View style={styles.priceOut}>
                            <Text style={styles.dollor}>$</Text>
                            <TextField placeholder="2,000" onChange={this.onChangeText.bind(this)} value={`${this.state.price}`} focusedOutTextInput={styles.priceText} focusedTextInput={styles.priceText} keyboardType='numeric' />
                            <Text style={styles.usd}>usd</Text>
                        </View>
                        <TouchableOpacity style={styles.sellText} onPress={this.setPrice.bind(this, this.props.data.price)}>
                            <Text style={styles.text}>Sell for ${this.props.data.price} </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={this.state.bottom ? [styles.btncheckout2, { marginBottom: keyboardHeight }] : styles.btncheckout}>
                    <GradientButton disable={!this.state.price} label={'Sell for $' + this.state.price} onPress={this.popupOpen.bind(this)} />
                </View>
                <PopupComp link={(popupDialog) => { this.popupDialog = popupDialog; }}
                    onPress={this.popupClose.bind(this)} label='CONGRATULATION' description="Your item is listed for sale and you can track it from your selling history"
                    btnText="Okay, Got it"
                />
                <Toast
                    ref="toast"
                    style={styles.toastText}
                    position='bottom'
                    fadeInDuration={750}
                    fadeOutDuration={5000}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
            </TouchableOpacity>
        )
    }
}
export default connect(state => ({}, mapDispatch))(SellingItem);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}

const styles = StyleSheet.create({
    main: {
        height: height - 70,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10
    },
    textout: {
        paddingTop: 15
    },
    content: {
        paddingTop: 15,
        marginBottom: 40
    },
    text: {
        borderWidth: 1,
        borderRadius: 10,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 5,
        fontSize: 12,
        paddingBottom: 5
    },
    btncheckout: {
        paddingTop: 10,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        //paddingBottom: 10,
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 20,
        width: '100%'
    },
    btncheckout2: {
        paddingTop: 10,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        //paddingBottom: 10,
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 20,
        width: '100%',
    },
    firstCon: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 24,
        backgroundColor: '#fff',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 10,
        paddingBottom: 10
    },
    mainCon: {
        marginTop: 30,
        alignItems: 'center'
    },
    textSellOut: {
        alignItems: "center"
    },
    textPrice: {
        fontSize: 12,
        color: 'grey'
    },
    priceOut: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    dollor: {
        fontSize: 15,
        paddingRight: 10
    },
    priceText: {
        fontSize: 40,
        maxWidth: '70%'
    },
    usd: {
        fontSize: 15
    },
    sellText: {
        alignItems: 'center',
        marginTop: 20,
        width: "45%"
    },
    toastText: {
        backgroundColor: '#000',
        width: 300
    }


})