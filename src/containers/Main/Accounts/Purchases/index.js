import React from 'react'
import { View, StyleSheet, Keyboard, ScrollView, Dimensions, Image, StatusBar } from 'react-native'
import { Icon, Container, Content } from 'native-base'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux';
import TextField from '../../../../components/TextField';
import Headers from '../../../../components/Navigation/Headers';
import ItemList from '../../../../components/Lists/ItemList';
import { inbox, purchasehistory } from '../../../../actions/MarketPlace';

var { height, width } = Dimensions.get('window');
class Purchases extends React.Component {
    constructor() {
        super()
        this.state = {
            list: [],
            loader: false,
        }
    }
    componentWillMount() {
        this.setState({ loader: true })
        this.props.dispatch(purchasehistory()).then(res => {
            let purchaseList = res.data, purchaseListArray = [];
            let purchaseListKeys = Object.keys(res.data)
            for (let i = 0; i < purchaseListKeys.length; i++) {
                let key = purchaseListKeys[i];
                purchaseListArray.push(purchaseList[key])
            }
            this.setState({ list: purchaseListArray })
            this.setState({ loader: false })
        })
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    goToPurchaseDetailPage(item) {
        Actions.PurchaseDetails({ data: item })
    }
    render() {
        return (
            <View style={styles.main} >
                {this.state.loader ?
                    <View style={styles.imageoutlines}>
                        <Image source={require('../../../../img/loader.gif')} style={styles.image} />
                    </View> :
                    <Container onPress={Keyboard.dismiss} accessible={false} activeOpacity={1}>
                        <Headers lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Purchase History" />
                        <StatusBar barStyle="dark-content"/>
                        <Content style={styles.scroll}>
                            <View style={styles.textfieldout}>
                                <View style={styles.searchicon}>
                                    <Icon name="search" style={styles.iconStyle} />
                                </View>
                                <View style={styles.textfieldmain}>
                                    <TextField focusedTextInput={styles.focusedTextInput} />
                                </View>
                            </View>
                            {this.state.list.map((item, key) => {
                                return (
                                    <View style={styles.map} key={key}>
                                        <ItemList name={item.name} price={item.price} image={item.thumbfile} activeOpacity={1} onPress={this.goToPurchaseDetailPage.bind(this, item)} />
                                    </View>
                                )
                            })}
                        </Content>
                    </Container>
                }
            </View>
        )
    }

}
export default connect(state => ({}, mapDispatch))(Purchases);
const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}

const styles = StyleSheet.create({
    main: {
        flex:1,
        justifyContent: 'center'
    },
    imageoutlines: {
        height: 70,
        width: 70,
        overflow: 'hidden',
        alignSelf: 'center',
    },
    firstCon: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        paddingTop: 24,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center'
    },
    secondcon: {
        width: '20%',
    },
    text: {
        width: '60%',
        alignItems: 'center'
    },
    textmain: {
        fontSize: 18,
    },
    iconcart: {
        width: '20%',
        alignItems: 'flex-end'
    },
    textDetails: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingBottom: 4
    },
    priceOut: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    price: {
        fontSize: 16
    },
    scroll: {
        height: height,
        backgroundColor: '#fff',
    },
    map: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    imgout: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)'
    },
    imgcont: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '30%',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 10
    },
    imgmain: {
        height: 60,
        width: 60,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    textout: {
        width: '45%',
        paddingLeft: 10,
        justifyContent: 'center'
    },
    textdetail: {
        fontSize: 12,
        paddingBottom: 3,
        color: 'rgba(0,0,0,0.4)'
    },
    touchableOut: {
        width: '25%',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    icon: {
        fontSize: 26
    },
    priceOut: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    textfieldout: {
        backgroundColor: '#f0f1f3',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        borderRadius: 10,
        height: 35,
    },
    searchicon: {
        width: '10%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textfieldmain: {
        width: '90%',
        justifyContent: 'center',
        height: 35
    },
    focusedTextInput: {
        fontSize: 14
    },
    iconStyle: {
        fontSize: 22
    }
})