import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Keyboard, AsyncStorage, StatusBar } from 'react-native'
import { Icon } from 'native-base'
import TextField from '../../../../components/TextField';
import GradientButton from '../../../../components/Buttons/GradientButton'
import { Actions } from 'react-native-router-flux'
import { logout } from '../../../../actions/Auth'
import Headers from '../../../../components/Navigation/Headers'
import { connect } from 'react-redux'
import { accountDetails } from '../../../../actions/MarketPlace';
class Profile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {
                username: '',
                email: '',
                password: '',
            },
            loader: false,
            account: {}
        }
    }

    componentWillMount() {
        this.props.dispatch(accountDetails()).then(res => {
            let { user } = this.state;
            user.username = res.name;
            user.email = res.email
            this.setState({ user })
        })
    }
    onChange(key, event) {
        const { user } = this.state
        user[key] = event
        this.setState({ user })
    }
    logout() {
        this.setState({ loader: true })
        this.props.dispatch(logout()).then(res => {
        console.log("signout ho gya")
        console.log(res.result,"result aaya")
            this.setState({ loader: false })
            if (res.result == 'success') {
                AsyncStorage.removeItem('isLoggedin')
                Actions.WelcomePage()
            }
        })
    }
    changePassword() {
        Actions.ChangePassword()
    }
    render() {
        let { user } = this.state
        return (
            <TouchableOpacity onPress={Keyboard.dismiss} accessible={false} activeOpacity={1} style={styles.main}>
                <Headers lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Profile" />
                <StatusBar barStyle='dark-content' />
                <View style={styles.uppercon}>
                    <View style={styles.userdata}>
                        <View style={styles.details}>
                            <Icon name="ios-person-outline" style={styles.icon} />
                            <TextField
                                focusedTextInput={styles.focusedTextInput}
                                focusedOutTextInput={styles.focusedOutTextInput}
                                placeholder={"Enter Username"}
                                value={user.username}
                                onChange={this.onChange.bind(this, 'username')}
                            />
                        </View>
                        <View style={styles.details}>
                            <Icon name="ios-mail-outline" style={styles.icon} />
                            <TextField
                                focusedTextInput={styles.focusedTextInput}
                                focusedOutTextInput={styles.focusedOutTextInput}
                                placeholder={"Enter Email"}
                                value={user.email}
                                onChange={this.onChange.bind(this, 'email')}
                            />
                        </View>
                        {/* <View style={styles.details}>
                            <Icon name="ios-mail-outline" style={styles.icon} />
                            <TextField
                                focusedTextInput={styles.focusedTextInput}
                                focusedOutTextInput={styles.focusedOutTextInput}
                                secureTextEntry={true}
                                placeholder="Enter New Email"
                                onChange={this.onChange.bind(this, 'newemail')}
                            />
                        </View> */}
                        <View style={styles.details}>
                        <Icon name="ios-lock-outline" style={styles.icon} />
                            <TextField
                                focusedTextInput={styles.focusedTextInput}
                                focusedOutTextInput={styles.focusedOutTextInput}
                                secureTextEntry={true}
                                placeholder="Password"
                                onChange={this.onChange.bind(this, 'password')}
                            />
                        </View>
                    </View>
                </View>
                <View style={styles.lowercon}>
                    <View style={styles.btnout}>
                        <GradientButton label="Save Profile" />
                    </View>
                    <View style={styles.btnout}>
                        <TouchableOpacity style={styles.btn2} onPress={this.changePassword.bind(this)}>
                            <Text style={styles.btntext2}>Change Password</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btnout}>
                        <TouchableOpacity style={styles.btn2} onPress={this.logout.bind(this)}>
                            <Text style={styles.btntext2}>{this.state.loader ? 'loading...' : 'Sign Out'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
export default connect(state => ({}, mapDispatch))(Profile);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}
const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    uppercon: {
        flex: 2,
        backgroundColor: 'white',
    },
    lowercon: {
        marginBottom: 15,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconcart: {
        alignItems: 'flex-end',
        width: '20%'
    },
    firstCon: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 24,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'white',
        marginBottom: 10
    },
    iconback: {
        width: '20%',
    },
    textmain: {
        fontSize: 20,
        paddingTop: 5,
    },
    text: {
        width: '60%',
        alignItems: 'center',
    },
    details: {
        paddingLeft: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    textdetail: {
        fontSize: 20
    },
    userdata: {
        backgroundColor: 'white',
    },
    btn: {
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn2: {
        borderRadius: 10,
        borderWidth: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 20
    },
    linear: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 50,
        borderRadius: 10
    },
    btnout: {
        paddingLeft: 20,
        paddingRight: 20,
        width: '100%'
    },
    btntext: {
        color: 'white',
        fontSize: 20
    },
    btntext2: {
        fontSize: 16
    },
    focusedTextInput: {
        width: '100%',
        fontSize: 16,
        paddingTop: 15,
        paddingBottom: 15,
        paddingRight: 50
    },
    focusedOutTextInput: {
        width: '100%',
        fontSize: 16,
        paddingTop: 15,
        paddingBottom: 15,
        paddingRight: 50
    },
    icon: {
        paddingRight: 10,
        color: '#60aef7',
        paddingTop: 15,
        paddingBottom: 15,
        fontSize: 22
    }
})