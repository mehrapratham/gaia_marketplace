import React from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import {  Tabs, Tab } from 'native-base';
import Sold from './Sold';
import ActiveSelling from './ActiveSelling';
import Headers from '../../../../components/Navigation/Headers';

export default class Selling extends React.Component {
    constructor() {
        super()
        this.state = {
            item: [
                {
                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {

                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {
                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {

                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {

                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {

                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {

                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                },
                {

                    name: 'Heart Container',
                    image: '../../img/game.png',
                    dollar: '738',
                    rarity: 'Ultra Rare',
                    date: 'Jul 27, 2018'
                }
            ]
        }
    }
    

    render() {
        const { item, currentTab } = this.state
        return (
            <View style={styles.main}>
                <StatusBar barStyle="dark-content"/>
                <Headers lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Selling" />
                <Tabs style={styles.tabColor} activeTabStyle={styles.activeTabColor} tabBarUnderlineStyle={styles.tabBarColor}>
                    <Tab heading='Active' activeTextStyle={styles.btnTabColor} textStyle={styles.textColor} activeTabStyle={styles.colorWhite} tabStyle={styles.colorWhite} style={styles.colorWhite}>
                        <ActiveSelling list={item} />
                    </Tab>
                    <Tab heading='Sold' activeTextStyle={styles.btnTabColor} textStyle={styles.textColor} activeTabStyle={styles.colorWhite} tabStyle={styles.colorWhite} style={styles.colorWhite}>
                        <Sold list={item} />
                    </Tab>
                </Tabs>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    icon: {
        fontSize: 26
    },
    tabColor: {
        backgroundColor: '#fff'
    },
    activeTabColor: {
        backgroundColor: '#fff',
        color: '#fff'
    },
    tabBarColor: {
        backgroundColor: '#6580ea'
    },
    btnTabColor: {
        color: '#6580ea'
    },
    textColor: {
        color: '#000'
    },
    colorWhite: {
        backgroundColor: '#fff'
    }

})