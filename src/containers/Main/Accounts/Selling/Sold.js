import React from 'react'
import { View, StyleSheet, Keyboard, ScrollView, Image } from 'react-native'
import { Icon } from 'native-base';
import { connect } from 'react-redux'
import TextField from '../../../../components/TextField';
import ItemList from '../../../../components/Lists/ItemList'
import { sellingList } from '../../../../actions/MarketPlace';
class Sold extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            loader:false,
        }
    }
    componentWillMount() {
        this.setState({ loader: true })
        this.props.dispatch(sellingList('sold')).then(res => {
            let soldList = res.data, soldListArray = [];
            let soldListKeys = Object.keys(res.data)
            for (let i = 0; i < soldListKeys.length; i++) {
                let key = soldListKeys[i];
                soldListArray.push(soldList[key])
            }
            this.setState({ list: soldListArray })
            this.setState({ loader: false })
        })
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    render() {
        return (
            <View onPress={Keyboard.dismiss} style={styles.main} accessible={false} activeOpacity={1}>
                {this.state.loader ?
                    <View style={styles.imageoutlines}>
                        <Image source={require('../../../../img/loader.gif')} style={styles.image} />
                    </View> :
                    <ScrollView style={styles.scroll}>
                        <View style={styles.textfieldout}>
                            <View style={styles.searchicon}>
                                <Icon name="search" style={styles.iconStyle} />
                            </View>
                            <View style={styles.textfieldmain}>
                                <TextField focusedTextInput={styles.focusedTextInput} />
                            </View>
                        </View>
                        {this.state.list.map((item, key) => {
                            return (
                                <View style={styles.map}>
                                    <ItemList name={item.name} price={item.price} image={item.thumbfile} date={item.date} activeOpacity={1} />
                                </View>
                            )
                        })}
                    </ScrollView>
                }
            </View>
        )
    }
}

export default connect(state => ({}, mapDispatch))(Sold);
const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}

const styles = StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    imageoutlines: {
		height: 70,
		width: 70,
		overflow: 'hidden',
		alignItems: 'center',
		justifyContent: 'center'
	},
    imgout: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)'
    },
    imgcont: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '30%',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 10
    },
    imgmain: {
        height: 60,
        width: 60,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    textout: {
        width: '45%',
        paddingLeft: 10,
        justifyContent: 'center'
    },
    textDetails: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingBottom: 4
    },
    touchableOut: {
        width: '25%',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    price: {
        fontSize: 14,
        color: 'rgba(0,0,0,0.5)'
    },
    scroll: {
        backgroundColor: '#fff'
    },
    map: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    priceOut: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    textdetail: {
        fontSize: 12,
        paddingBottom: 3,
        color: 'rgba(0,0,0,0.5)'
    },
    textfieldout: {
        backgroundColor: '#f0f1f3',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        borderRadius: 10,
        height: 35,
    },
    searchicon: {
        width: '10%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textfieldmain: {
        width: '90%',
        justifyContent: 'center',
        height: 35
    },
    focusedTextInput: {
        fontSize: 14
    },
    iconStyle: {
        fontSize: 22
    }
})