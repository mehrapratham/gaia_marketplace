import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, StatusBar, Image, ScrollView, Dimensions, SafeAreaView } from 'react-native'
import { Icon, Drawer } from 'native-base'
import TextField from '../../components/TextField'
import { Actions } from 'react-native-router-flux';
import MenuList from '../../components/Lists/MenuList'
import Headers from '../../components/Navigation/Headers';
var { height, width } = Dimensions.get('window');
export default class SearchDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        },
        {
          name: 'Heart Container',
          image: '../../img/game.png',
          dollar: '738',
          like: '9k'
        }
      ],
      isOpened: false
    }
  }
  closeDrawer = () => {
    this.drawer._root.close()
    this.setState({ isOpened: false })
  };
  openDrawer() {
    if (this.state.isOpened) {
      this.closeDrawer()
    } else {
      this.drawer._root.open()
      this.setState({ isOpened: true })
    }
  };
  render() {
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        side="right"
        content={<View style={styles.contentOut}>
          <MenuList onPressDone={this.closeDrawer.bind(this)} />
        </View>}
        onClose={() => this.closeDrawer()}
        tapToClose={true}
        openDrawerOffset={0.2}>
        <View style={styles.flex1}>
          <StatusBar barStyle="dark-content" />
          <Headers lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Search Results" />
          <ScrollView style={styles.scroll}>
            <View style={styles.textfieldout}>
              <View style={styles.searchicon}>
                <Icon name="search" style={styles.iconStyle} />
              </View>
              <View style={styles.textfieldmain}>
                <TextField focusedTextInput={styles.focusedTextInput} />
              </View>
            </View>
            <View style={styles.map}>
              <View style={styles.drawerOut}>
                <View style={styles.width50}>
                  <Text style={styles.textColor}>35 results</Text>
                </View>
                <View style={styles.width50}>
                  <TouchableOpacity onPress={this.openDrawer.bind(this)}><Text style={styles.drawerText}>SORT AND FILTER</Text></TouchableOpacity>
                </View>
              </View>
              {this.state.item.map((item, key) => {
                return (
                  <TouchableOpacity style={styles.container} key={key} >
                    <View style={styles.imgcon}>
                      <View style={styles.imageoutline}>
                        <Image source={require('../../img/heart.png')} style={styles.image} />
                      </View>
                    </View>
                    <View style={styles.namecon}>
                      <View style={styles.nameinnercon}>
                        <Text style={styles.itemname}>Heart Container</Text>
                      </View>
                      <View style={styles.pricecon}>
                        <View style={styles.priceinnercon}>
                          <Text style={styles.price}>$738</Text>
                        </View>
                        <View style={styles.likecon}>
                          <Text style={styles.like}>9k</Text>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              })}
            </View>
          </ScrollView>
        </View>
      </Drawer>
    )
  }
}
const styles = StyleSheet.create({
  firstCon: {
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingBottom: 10,
    marginBottom: 10
  },
  secondcon: {
    width: '20%'
  },
  textcon: {
    width: '60%',
    alignItems: 'center'
  },
  icon: {
    marginRight: 20
  },
  textmain: {
    fontSize: 18,
    paddingTop: 5
  },
  iconcart: {
    width: '20%',
    alignItems: 'flex-end'
  },
  scroll: {
    backgroundColor: '#fff'
  },
  map: {
    alignItems: "center",
    flexDirection: "row",
    flexWrap: 'wrap',
    paddingLeft: 10,
  },
  container: {
    marginRight: 10,
    width: width / 2 - 15,
    marginBottom: 10,
    shadowColor: 'grey',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
  },
  imgcon: {
    paddingTop: 30,
    paddingBottom: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  imageoutline: {
    height: 70,
    width: 70,
    overflow: 'hidden'
  },
  image: {
    width: '100%',
    height: '100%'
  },
  namecon: {
    padding: 10,
    backgroundColor: '#fff',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  nameinnercon: {
    marginBottom: 5
  },
  itemname: {
    color: 'rgba(0,0,0,0.3)',
    fontSize: 12
  },
  pricecon: {
    flexDirection: 'row'
  },
  priceinnercon: {
    width: '50%'
  },
  price: {
    color: 'rgba(0,0,0,0.3)',
    fontSize: 12
  },
  likecon: {
    width: '50%',
    alignItems: 'flex-end'
  },
  like: {
    color: 'rgba(0,0,0,0.3)',
    fontSize: 12
  },
  flex1: {
    flex: 1
  },
  textfieldout: {
    backgroundColor: '#f0f1f3',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
    borderRadius: 10,
    height: 35,
  },
  searchicon: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textfieldmain: {
    width: '90%',
    justifyContent: 'center',
    height: 35
  },
  focusedTextInput: {
    fontSize: 18
  },
  iconStyle: {
    fontSize: 22
  },
  drawerOut: {
    width: '100%',
    flexDirection: 'row',
    paddingTop: 20,
    paddingBottom: 10,
    paddingBottom: 15
  },
  width50: {
    width: '50%'
  },
  drawerText: {
    color: '#6580ea',
    alignSelf: 'flex-end',
    marginRight: 10
  },
  textColor: {
    color: 'rgba(0,0,0,0.3)'
  },
  contentOut: {
    backgroundColor: '#fff',
    height: '100%'
  }
})