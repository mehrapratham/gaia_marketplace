import React from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import { Container, Content, Footer, FooterTab, Button, Icon } from 'native-base';
import Accounts from './Accounts'
import Alerts from './Alerts'
import MarketPlace from './MarketPlace'
import Inventory from './Inventory';
import {AsyncStorage } from 'react-native'

import { getFromAsyncStorage } from '../../components/Common/asyncStorage';


export default class Main extends React.Component {
	constructor() {
		super();
		/*
		* initial state variables
		*/
		this.state = {
			activeTabs: 1
		}
	}
	async componentWillMount() {
		let sid = await AsyncStorage.getItem('isLoggedin')
		console.log(sid,676767)
		// if (this.props.tab === 2) {
		// 	var id = this.props.tab
		// 	console.log(id,1212)
		// 	this.setState({ activeTabs: id })
		// }
		// else{
		// 	return null
		// }
	}
	/**
	* tab change function
	* @param tabs
	*/
	onChangeTabs(tabs) {
		this.setState({ activeTabs: tabs })
	}
	/*renter function for tabbing view*/
	render() {
		return (
			<Container>
				<View style={{ flex: 1 }}>
					{this.state.activeTabs == 1 && <MarketPlace />}
					{this.state.activeTabs == 2 && <Inventory />}
					{this.state.activeTabs == 3 && <Alerts />}
					{this.state.activeTabs == 4 && <Accounts />}
				</View>
				<Footer style={{ backgroundColor: "#FFF" }}>
					<FooterTab style={{ backgroundColor: "#FFF" }}>
						<Button active={this.state.activeTabs == 1} style={this.state.activeTabs == 1 ? styles.activeCon : styles.container} onPress={this.onChangeTabs.bind(this, 1)} >
							<Icon name="md-home" style={this.state.activeTabs == 1 ? styles.activeText : styles.textColor} />
						</Button>
						<Button active={this.state.activeTabs == 2} style={this.state.activeTabs == 2 ? styles.activeCon : styles.container} onPress={this.onChangeTabs.bind(this, 2)}>
							<Icon name="ios-download-outline" style={this.state.activeTabs == 2 ? styles.activeText : styles.textColor} />
						</Button>
						<Button active={this.state.activeTabs == 3} style={this.state.activeTabs == 3 ? styles.activeCon : styles.container} onPress={this.onChangeTabs.bind(this, 3)}>
							<Icon name="ios-notifications-outline" style={this.state.activeTabs == 3 ? styles.activeText : styles.textColor} />
						</Button>
						<Button active={this.state.activeTabs == 4} style={this.state.activeTabs == 4 ? styles.activeCon : styles.container} onPress={this.onChangeTabs.bind(this, 4)}>
							<Icon name="ios-person-outline" style={this.state.activeTabs == 4 ? styles.activeText : styles.textColor} />
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		)
	}
}
const styles = StyleSheet.create({
	activeCon: {
		backgroundColor: '#fff',
		borderBottomWidth: 4,
		borderColor: 'blue'
	},
	container: {
		backgroundColor: '#fff'
	},
	activeText: {
		color: '#6580ea'
	},
	textColor: {
		color: 'rgba(0,0,0,0.5)'
	}
})
