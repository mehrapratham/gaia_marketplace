import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { LinearGradient } from 'expo';
export default class WelcomePage extends React.Component{
	/*render function for welcome screen*/
	render(){
		return(
			<View style={styles.container}>
				<LinearGradient
		          colors={['#66e8dc', '#6580ea']}
		          style={styles.container}
		          start={[-0.6, 1]} end={[-0.1, 1.7]}
		        >
		        	<View style={styles.firstCon}>
		        		<View style={styles.innerCon}>
		        		</View>
		        	</View>
		        	<View style={styles.secondCon}>
		        		<View style={styles.innersecondCon}>
		        			<Text style={styles.center}>Buy and sell items with other players on Gaia</Text>
		        		</View>
		        		<View style={styles.secondCon}>
		        			<TouchableOpacity style={styles.buttnStyle} onPress={() => {Actions.LoginWithFb();}}>
		        				<Text style={styles.buttnColor}>Get Started</Text>
		        			</TouchableOpacity>
		        		</View>
		        	</View>
				</LinearGradient>
			</View>
		)
	}
} 

/*welcome page css as json*/
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	firstCon: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	innerCon: {
		backgroundColor: '#fff',
		width: 220,
		height: 220,
	},
	secondCon: {
		flex: 1,
		justifyContent: 'flex-end'
	},
	center: {
		textAlign: 'center',
		fontSize: 16,
		color: '#fff',
		paddingLeft: 40,
		paddingRight: 40
	},
	buttnColor: {
		color: '#6580ea'
	},
	buttnStyle: {
		height: 50,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 10,
		margin: 20
	},
	innersecondCon: {
		flex: 1,
		justifyContent: 'center'
	}

})