import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native'
import TextField from '../../../components/TextField'
import { Actions } from 'react-native-router-flux';
import { IsValidForm } from '../../../components/Common/validation'
import { resetPassword, getFromLocalStorage } from '../../../actions/Auth'
import { connect } from 'react-redux'
import Toast from 'react-native-easy-toast'
import GradientButton from '../../../components/Buttons/GradientButton'
class ResetPassword extends React.Component {
	constructor(props) {
		super(props);
		/*
		* initial state variables
		*/
		this.state = {
			user: {
				code: '',
				password: '',
				confirm_password: ''
			},
			errors: {},
			loader: false
		}
	}

	/**
	* go back to previous screen
	*/
	goBack() {
		Actions.pop()
	}

	/**
	* on value change event
	* @param key
	* @param event
	*/
	onChange(key, event) {
		const { user } = this.state
		user[key] = event
		this.setState({ user: this.state.user })
	}

	/**
	* submit reset password data
	* @param code
	* @param password
	* @param confirm password
	*/
	async onSubmit() {
		let emailFromStorage = await this.props.dispatch(getFromLocalStorage('resetEmail'))

		let fields = ['code', 'password', 'confirm_password'];
		let { user } = this.state;
		let formValidation = IsValidForm(fields, user)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			if (user.password != user.confirm_password) {
				this.refs.toast.show("Password and confirm password doesn't match")
			}
			else {
				let data = { key: user.code, password: user.password, email: emailFromStorage.email }
				this.setState({ loader: true })
				this.props.dispatch(resetPassword(data)).then(res => {
					this.setState({ loader: false })
					if (res.result == 'success') {
						this.setState({ loader: false })
						user = { code: '', password: '', confirm_password: '' }
						this.setState({ user });
						this.refs.toast.show(res.message)
						Actions.Login();
					}
					else {
						this.setState({ loader: false })
						return (this.refs.toast.show(res.message))
					}
				})
			}
		} else {
			this.refs.toast.show('Some field are missing')
		}
	}

	/*reset password render screen function*/
	render() {
		return (
			<TouchableOpacity style={styles.container} onPress={Keyboard.dismiss} accessible={false} activeOpacity={1}>
				<View style={styles.firstView}>
					<View style={styles.innerView}>
						<Text style={styles.firstText}>Reset password</Text>
						<TextField placeholder="Enter Authorization Code" focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} onChange={this.onChange.bind(this, 'code')} />
					</View>
					<View style={styles.innerSecondView}>
						<TextField secureTextEntry={true} focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="New Password" onChange={this.onChange.bind(this, 'password')} />
						<TextField secureTextEntry={true} placeholder="Confirm New Password" focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} onChange={this.onChange.bind(this, 'confirm_password')} />
					</View>
				</View>
				<View style={styles.outerView}>
					<GradientButton label={!this.state.loader ? "Resest Password" : "loading..."} onPress={this.onSubmit.bind(this)} />
				</View>
				<Toast
					ref="toast"
					style={{ backgroundColor: '#000', width: 300 }}
					position='bottom'
					fadeInDuration={750}
					fadeOutDuration={5000}
					opacity={0.8}
					textStyle={{ color: '#fff' }}
				/>
			</TouchableOpacity>
		)
	}
}

export default connect(state => ({}, mapDispatch))(ResetPassword);

const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}

/*reset password page css as json*/
const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	focusedTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: '#6580ea'
	},
	focusedOutTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	firstText: {
		fontSize: 24,
		fontWeight: 'bold',
		textAlign: 'center',
		marginBottom: 30
	},
	firstView: {
		flex: 2,
		paddingLeft: 20,
		paddingRight: 20
	},
	innerView: {
		flex: 1,
		justifyContent: 'flex-end'
	},
	outerView: {
		flex: 1,
		justifyContent: 'center',
		paddingLeft: 20,
		paddingRight: 20
	},
	innerSecondView: {
		flex: 1,
		justifyContent: 'center',
	}
})