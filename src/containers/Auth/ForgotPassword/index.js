import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native'
import { Icon } from 'native-base';
import TextField from '../../../components/TextField'
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-easy-toast'
import { lostPassword, saveToLocalStorage } from '../../../actions/Auth'
import { connect } from 'react-redux'
import GradientButton from '../../../components/Buttons/GradientButton'
class ForgotPassword extends React.Component {
	constructor(props) {
		super(props);
		/*
		* initial state variables
		*/
		this.state = {
			email: '',
			errors: {},
			loader: false
		}
	}

	/**
	* go back to previous screen
	*/
	goBack() {
		Actions.pop()
	}

	/**
	* on value change event
	* @param key
	* @param event
	*/
	onChange(key, event) {
		this.setState({ email: event })
	}

	/**
	* submit forgot password data
	* @param email
	*/
	onSubmit() {
		if (this.state.email == '') {
			this.refs.toast.show('Email is missing')
		} else {
			let data = { email: this.state.email }
			this.setState({ loader: true })
			this.props.dispatch(lostPassword(data)).then(res => {
				console.log(res,"hello")
				this.setState({ loader: false })
				if (res.result == 'success') {
					this.setState({ loader: false })
					this.refs.toast.show(res.message)
					let dataToString = JSON.stringify(data)
					this.props.dispatch(saveToLocalStorage('resetEmail', dataToString))
					Actions.ResetPassword();
				}
			})
		}
	}

	/*forgot password render screen function*/
	render() {
		return (
			<TouchableOpacity style={styles.container} onPress={Keyboard.dismiss} accessible={false} activeOpacity={1}>
				<TouchableOpacity style={styles.iconStyle} onPress={this.goBack.bind(this)}><Icon name="ios-arrow-back" /></TouchableOpacity>
				<View style={styles.firstView}>
					<View style={styles.innerView}>
						<Text style={styles.firstText}>Forgot password</Text>
						<Text style={styles.secondText}>Enter your email to receive instructions on how to reset your password</Text>
					</View>
					<View style={styles.innerSecondView}>
						<TextField placeholder="Email" focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} onChange={this.onChange.bind(this, 'email')} />
					</View>
				</View>
				<View style={styles.outerView}>
					<GradientButton label={!this.state.loader ? "Send Request" : "loading..."} onPress={this.onSubmit.bind(this)} />
				</View>
				<Toast
					ref="toast"
					style={{ backgroundColor: '#000', width: 300 }}
					position='bottom'
					fadeInDuration={750}
					fadeOutDuration={5000}
					opacity={0.8}
					textStyle={{ color: '#fff' }}
				/>
			</TouchableOpacity>
		)
	}
}

export default connect(state => ({}, mapDispatch))(ForgotPassword);


const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}

/*forgot password css as json*/
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	focusedTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: '#6580ea'
	},
	focusedOutTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	iconStyle: {
		marginLeft: 10,
		paddingTop: 24
	},
	firstView: {
		flex: 2,
		paddingLeft: 20,
		paddingRight: 20
	},
	firstText: {
		fontSize: 24,
		fontWeight: 'bold',
		textAlign: 'center',
		marginBottom: 30
	},
	innerView: {
		flex: 1,
		justifyContent: 'flex-end'
	},
	innerSecondView: {
		flex: 1,
		justifyContent: 'center'
	},
	secondText: {
		textAlign: 'center',
		fontSize: 14
	},
	outerView: {
		flex: 1,
		justifyContent: 'center',
		paddingLeft: 20,
		paddingRight: 20
	}
})