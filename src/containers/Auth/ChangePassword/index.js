import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native'
import { Icon } from 'native-base'
import Headers from '../../../components/Navigation/Headers';
import TextField from '../../../components/TextField';
import GradientButton from '../../../components/Buttons/GradientButton';
import { changePass } from '../../../actions/MarketPlace';
import { connect } from 'react-redux'
import Toast from 'react-native-easy-toast'
import PopupComp from '../../../components/Popup/Popup';
import { Actions } from 'react-native-router-flux';
import { IsValidForm } from '../../../components/Common/validation';


class ChangePassword extends React.Component {
    constructor() {
        super()
        this.state = {
            user: {
                email: '',
                oldPassword: '',
                newPassword: ''
            },
            loader: false,
            bottom: false,
            errors: {},
        }
    }
    componentWillMount() {
        // this.props.dispatch(accountDetails()).then(res => {
        //     let { user } = this.state;
        //     user.username = res.name;
        //     user.email = res.email
        //     this.setState({ user })
        // })
    }
    onChange(key, event) {
        const { user } = this.state
        user[key] = event
        this.setState({ user })
    }
    onSubmit() {
        let fields = ['email', 'oldPassword', 'newPassword']
        let formValidation = IsValidForm(fields, this.state.user)
        this.setState({ errors: formValidation.errors })
        if (formValidation.validate) {
            let { email, oldPassword, newPassword } = this.state.user
            this.props.dispatch(changePass(email, oldPassword, newPassword)).then(res => {
                console.log(res, "9876987")
            })
            this.popupDialog.show()
        } else {
            this.refs.toast.show('Some field are missing')
        }
        this.setState({ bottom: false })




    }
    popupClose() {
        Actions.Profile();
        this.popupDialog.dismiss();
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Headers lefticon="ios-arrow-back" righticon='ios-cart-outline' label="Change Password" />
                <View style={styles.main}>
                    <View style={styles.details}>
                        <Icon name="ios-person-outline" style={styles.icon} />
                        <TextField
                            focusedTextInput={styles.focusedTextInput}
                            focusedOutTextInput={styles.focusedOutTextInput}
                            placeholder={"Enter Email"}
                            value={this.state.user.email}
                            onChange={this.onChange.bind(this, 'email')}
                        />
                    </View>
                    <View style={styles.details}>
                        <Icon name="ios-lock-outline" style={styles.icon} />
                        <TextField
                            focusedTextInput={styles.focusedTextInput}
                            focusedOutTextInput={styles.focusedOutTextInput}
                            placeholder={"Enter Old Password"}
                            secureTextEntry={true}
                            value={this.state.user.oldPassword}
                            onChange={this.onChange.bind(this, 'oldPassword')}
                        />
                    </View>
                    <View style={styles.details}>
                        <Icon name="ios-lock-outline" style={styles.icon} />
                        <TextField
                            focusedTextInput={styles.focusedTextInput}
                            focusedOutTextInput={styles.focusedOutTextInput}
                            secureTextEntry={true}
                            placeholder="Enter New Password"
                            value={this.state.user.newPassword}
                            onChange={this.onChange.bind(this, 'newPassword')}
                        />
                    </View>
                </View>
                <View style={styles.btnout}>
                    <GradientButton label="Change Password" onPress={this.onSubmit.bind(this)} />
                </View>
                <PopupComp link={(popupDialog) => { this.popupDialog = popupDialog; }}
                    onPress={this.popupClose.bind(this)} label='CONGRATULATION' description="Your Passowrd has been Changed"
                    btnText="Okay, Got it"
                />
                <Toast
                    ref="toast"
                    style={{ backgroundColor: '#000', width: 300 }}
                    position='bottom'
                    fadeInDuration={750}
                    fadeOutDuration={5000}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
            </View>
        )
    }
}

export default connect(state => ({}, mapDispatch))(ChangePassword);

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, dispatch);
    return allActionProps;
}
const styles = StyleSheet.create({
    main: {
        backgroundColor: '#fff',
        flex: 1
    },
    details: {
        paddingLeft: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#e5e5e5',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    focusedTextInput: {
        width: '100%',
        fontSize: 16,
        paddingTop: 15,
        paddingBottom: 15,
        paddingRight: 50
    },
    focusedOutTextInput: {
        width: '100%',
        fontSize: 16,
        paddingTop: 15,
        paddingBottom: 15,
        paddingRight: 50
    },
    btnout: {
        position: 'absolute',
        bottom: 20,
        flex: 1,
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20
    },
    icon: {
        paddingRight: 10,
        color: '#60aef7',
        paddingTop: 15,
        paddingBottom: 15,
        fontSize: 20
    }

})