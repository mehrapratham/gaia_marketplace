import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native'
import { Icon } from 'native-base';
import TextField from '../../../components/TextField'
import { login } from '../../../actions/Auth'
import { connect } from 'react-redux'
import { IsValidForm } from '../../../components/Common/validation'
import Toast from 'react-native-easy-toast'
import { Actions } from 'react-native-router-flux';
import GradientButton from '../../../components/Buttons/GradientButton'
const FBSDK = require('react-native-fbsdk');
const {
	LoginButton,
	AccessToken
} = FBSDK;
class Login extends React.Component {
	constructor(props) {
		super(props);
		/*
		* initial state variables
		*/
		this.state = {
			user: {
				email: '',
				password: ''
			},
			loader: false,
			bottom: false,
			keyHeight: 0
		}
	}

	/**
	* go back to previous screen
	*/
	goBack() {
		Actions.pop()
	}

	/**
	* on value change event
	* @param key
	* @param event
	*/
	onChange(key, event) {
		const { user } = this.state
		user[key] = event
		this.setState({ user })
	}

	/**
	* submit login detail to login
	* @param email
	* @param password
	*/
	onSubmit(e) {
		let fields = ['email', 'password']
		let formValidation = IsValidForm(fields, this.state.user)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			let data = this.state.user;
			this.setState({ loader: true })
			this.props.dispatch(login(data)).then(res => {
				this.setState({ loader: false })
				if (res.result == 'success') {
					this.setState({ loader: false })
					Actions.Main();
				}
				if (res.message) {
					console.log(res,"000000")
					this.setState({ loader: false })
					return (this.refs.toast.show(res.message))
				}
			})
		} else {
			this.refs.toast.show('Some field are missing')
		}
		this.setState({ bottom: false })
		Keyboard.dismiss()
	}
	componentWillMount() {
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
	}
	componentWillUnmount() {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}
	_keyboardDidShow = (e) => {
		this.setState({ bottom: true })
		var keyboardHeight = e.endCoordinates.height
		this.setState({ keyHeight: keyboardHeight })
	}
	_keyboardDidHide = () => {
		this.setState({ bottom: false })
	}
	onFocusIn(bool) {
		if (bool) {
			this.setState({ bottom: true })
		}
	}
	touchOut() {
		Keyboard.dismiss();
		this.setState({ bottom: false })
	}
	render() {
		var keyboardHeight = this.state.keyHeight
		return (
			<TouchableOpacity style={styles.container} onPress={this.touchOut.bind(this)} accessible={false} activeOpacity={1}>
				<View style={styles.arrowOut}>
					<TouchableOpacity style={styles.iconStyle} onPress={this.goBack.bind(this)}><Icon name="ios-arrow-back" /></TouchableOpacity>
				</View>
				<View style={this.state.bottom ? [styles.form, { marginTop: 40 }] : styles.form}>
					<TextField onFocus={this.onFocusIn.bind(this)} focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="Email or username" onChange={this.onChange.bind(this, 'email')} />
					<TextField onFocus={this.onFocusIn.bind(this)} keyboardismiss={true} secureTextEntry={true} placeholder="Password" focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} onChange={this.onChange.bind(this, 'password')} />
					<View style={styles.btnForgot}>
						<TouchableOpacity style={styles.btnForgotoOut} onPress={() => { Actions.ForgotPassword(); }}><Text style={styles.forgotText}>Forgot Password?</Text></TouchableOpacity>
					</View>
				</View>
				<View style={styles.btnJoinOut}>
					<View style={this.state.bottom ? [styles.btnJoin, { marginBottom: keyboardHeight - 20 }] : styles.btnJoin}>
						<GradientButton label={!this.state.loader ? "Log In" : "loading..."} onPress={this.onSubmit.bind(this)} />
					</View>
					<View style={styles.joinNowText}>
						<Text style={styles.textCenter}><Text style={styles.textColor}>Not a member?</Text><Text style={styles.joinText} onPress={() => { Actions.Signup() }}>  join now</Text></Text>
					</View>
				</View>
				<Toast
					ref="toast"
					style={{ backgroundColor: '#000', width: 300 }}
					position='bottom'
					fadeInDuration={750}
					fadeOutDuration={5000}
					opacity={0.8}
					textStyle={{ color: '#fff' }}
				/>
			</TouchableOpacity>
		)
	}
}
export default connect(state => ({}, mapDispatch))(Login);
const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}
/*login page css as json*/
const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop:34
	},
	arrowOut: {
		width: 40
	},
	form: {
		justifyContent: 'center',
		paddingLeft: 20,
		paddingRight: 20,
		marginTop: 100
	},
	focusedTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: '#6580ea'
	},
	focusedOutTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	forgotText: {
		alignSelf: 'flex-end',
		color: '#6580ea',
		marginTop: 20
	},
	joinText: {
		color: '#6580ea'
	},
	textCenter: {
		textAlign: 'center'
	},
	textColor: {
		color: 'rgba(0,0,0,0.5)'
	},
	iconStyle: {
		marginLeft: 10,
	},
	btnForgot: {
		alignItems: 'flex-end'
	},
	btnForgotoOut: {
		width: 'auto'
	},
	btnJoinOut: {
		paddingLeft: 20,
		paddingRight: 20,
		position: 'absolute',
		bottom: 20,
		width: '100%',
	},
	btnJoin: {

	},
	joinNowText: {
		marginTop: 5,
		justifyContent: 'center'
	},
})