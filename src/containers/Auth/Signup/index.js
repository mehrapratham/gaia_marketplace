import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Keyboard, KeyboardAvoidingView, ScrollView } from 'react-native'
import { Icon, Content, Container } from 'native-base';
import TextField from '../../../components/TextField'
import { signup } from '../../../actions/Auth'
import { connect } from 'react-redux'
import { IsValidForm } from '../../../components/Common/validation'
import Toast from 'react-native-easy-toast'
import { Actions } from 'react-native-router-flux';
const FBSDK = require('react-native-fbsdk');
import GradientButton from '../../../components/Buttons/GradientButton'
const {
	LoginButton,
	AccessToken
} = FBSDK;
class Signup extends React.Component {
	constructor(props) {
		super(props);
		/*
		* initial state variables
		*/
		this.state = {
			user: {
				name: '',
				email: '',
				password: ''
			},
			errors: {},
			loader: false
		}
	}

	/**
	* go back to previous screen
	*/
	goBack() {
		Actions.pop()
	}

	/**
	* on value change event
	* @param key
	* @param event
	*/
	onChange(key, event) {
		const { user } = this.state
		user[key] = event
		this.setState({ user: this.state.user })
	}

	/**
	* submit signup data
	* @param name
	* @param email
	* @param password
	*/
	onSubmit() {
		let fields = ['name', 'email', 'password']
		let formValidation = IsValidForm(fields, this.state.user)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			let data = this.state.user;
			this.setState({ loader: true })
			this.props.dispatch(signup(data)).then(res => {
				this.setState({ loader: false })
				if (res.result == 'success') {
					this.setState({ loader: false })
					Actions.Main();
				}
				if (res.message) {
					this.setState({ loader: false })
					return (this.refs.toast.show(res.message))
				}
			})
		} else {
			this.refs.toast.show('Some field are missing')
		}
	}
	// async logIn() {
	//   const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('206759976615213', {
	//       permissions: ['public_profile'],
	//     });
	//   if (type === 'success') {
	//     // Get the user's name using Facebook's Graph API
	//     const response = await fetch(
	//       `https://graph.facebook.com/me?access_token=${token}`);
	//     // Alert.alert(
	//     //   'Logged in!',
	//     //   `Hi ${(await response.json()).name}!`,
	//     // );
	//   }
	// }

	/*signup render screen function*/
	render() {
		return (
			<Content contentContainerStyle={styles.content}>
				<Container>
					<TouchableOpacity style={styles.container} onPress={Keyboard.dismiss} accessible={false} activeOpacity={1}>
						<KeyboardAvoidingView style={styles.innerCon} behavior="padding" enabled>
							<View style={styles.Main}>
								<View style={styles.iconOut}>
									<TouchableOpacity style={styles.iconStyle} onPress={this.goBack.bind(this)}><Icon name="ios-arrow-back" /></TouchableOpacity>
								</View>
								<View style={styles.screenWidth}>
									<TextField focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="username" />
									<TextField focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="name" onChange={this.onChange.bind(this, 'name')} />
									<TextField focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="Email" onChange={this.onChange.bind(this, 'email')} />
									<TextField focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="Password" onChange={this.onChange.bind(this, 'password')} />
									<TextField focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="mm/dd/yyyy" />
								</View>
							</View>
							<View style={styles.secondCon}>
								<Text style={styles.textSize}>By clicking Sign Up, you agree with our</Text>
								<Text style={styles.blueColor}>Terms and Services</Text>
								<View style={styles.btnOut}>
									<GradientButton label={!this.state.loader ? "Sign Up" : "loading..."} onPress={this.onSubmit.bind(this)} />
								</View>
								<Text style={{ textAlign: 'center' }}><Text style={styles.texts}>Already registered?</Text><Text style={styles.colors} onPress={() => { Actions.Login() }}>  login</Text></Text>
							</View>
						</KeyboardAvoidingView>
						<Toast
							ref="toast"
							style={{ backgroundColor: '#000', width: 300 }}
							position='bottom'
							fadeInDuration={750}
							fadeOutDuration={5000}
							opacity={0.8}
							textStyle={{ color: '#fff' }}
						/>
					</TouchableOpacity>
					
				</Container>
			</Content>
		)
	}
}
export default connect(state => ({}, mapDispatch))(Signup);

const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}

/*signup page css as json*/
const styles = StyleSheet.create({
	content:{
		flex:1
	},
	Main:{
		paddingTop: 34 
	},
	container: {
		flex:1
	},
	innerCon: {
		flex:1
	},
	text: {
		fontSize: 26,
		textAlign: 'center',
		marginBottom: 20
	},
	focusedTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: '#6580ea'
	},
	focusedOutTextInput: {
		height: 65,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	screenWidth: {
		paddingLeft: 20,
		paddingRight: 20
	},
	blueColor: {
		color: '#6580ea',
		textAlign: 'center',
		marginTop: 10
	},
	textSize: {
		textAlign: 'center',
		marginTop: 20
	},
	gradientView: {
		marginTop: 20,
		marginBottom: 20,
		width: '100%',
		borderWidth: 2
	},
	texts: {
		fontSize: 14,
		color: 'rgba(0,0,0,0.6)'
	},
	colors: {
		color: '#6580ea',
		fontSize: 14
	},
	iconStyle: {
		marginLeft: 10
	},
	iconOut:{ 
		width: 30 ,
	},
	secondCon:{
		position: 'absolute', 
		bottom: 20, 
		paddingLeft: 10, 
		paddingRight: 10, 
		alignSelf: 'center', 
		width: '100%' 
	},
	btnOut:{ 
		width: '100%', 
		padding: 20 
	}	
})