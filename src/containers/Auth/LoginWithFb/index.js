import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { LinearGradient } from 'expo';
import GradientButton from '../../../components/Buttons/GradientButton'
export default class LoginWithFb extends React.Component {
	/**
	* go to sign up screen
	*/
	goToSignUp() {
		Actions.Signup();
	}

	/*render function for welcome screen*/
	render() {
		return (
			<View style={styles.container}>
				<LinearGradient
					colors={['#66e8dc', '#6580ea']}
					style={styles.container}
					start={[-0.6, 1]} end={[-0.1, 1.7]}
				>
					<View style={styles.firstCon}>
						<View style={styles.innerCon}>
						</View>
					</View>
				</LinearGradient>
				<View style={styles.container}>
					<View style={styles.secondView}>
						<View style={styles.bottom}>
							<Text style={styles.textSize}>Get started with </Text>
						</View>
						<View style={styles.bottom}>
							<TouchableOpacity style={styles.buttn}>
								<Text style={styles.textSize2}>Facebook</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.bottom}>
							<Text style={styles.textSize}>Or signup with</Text>
						</View>
						<View style={styles.bottom}>
							<GradientButton label="Email" onPress={() => { Actions.Signup() }} />
						</View>
						<View style={styles.bottom}>
							<Text><Text style={styles.textSize}>Already registered?</Text><Text style={styles.colors} onPress={() => { Actions.Login() }}>  login</Text></Text>
						</View>
					</View>
				</View>
			</View>
		)
	}
}

/*welcome page css as json*/
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	firstCon: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	innerCon: {
		backgroundColor: '#fff',
		width: 180,
		height: 180,

	},
	secondView: {
		paddingLeft: 30,
		paddingRight: 30,
		paddingTop: 20,
		paddingBottom: 20
	},
	textSize: {
		fontSize: 15,
		color: 'rgba(0,0,0,0.6)'
	},
	textSize2: {
		fontSize: 15,
		color: '#fff'
	},
	bottom: {
		marginBottom: 20
	},
	buttn: {
		backgroundColor: '#3b5998',
		height: 50,
		borderRadius: 10,
		alignItems: 'center',
		justifyContent: 'center'
	},
	colors: {
		color: '#6580ea'
	}

})