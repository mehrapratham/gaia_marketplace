import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Keyboard } from 'react-native'
import TextField from '../../../components/TextField'
import { Actions } from 'react-native-router-flux';
import { IsValidForm } from '../../../components/Common/validation'
import Toast from 'react-native-easy-toast'
import { editProfile, getProfileData } from '../../../actions/Auth'
import { connect } from 'react-redux'
class EditProfile extends React.Component {
	constructor(props) {
		super(props);
		/*
		* initial state variables
		*/
		this.state = {
			user: {
				email: '',
				password: '',
				confirm_password: ''
			},
			errors: {},
		}
	}

	/**
	* go back to previous screen
	*/
	goBack() {
		Actions.pop()
	}

	/**
	* on value change event
	* @param key
	* @param event
	*/
	onChange(key, event) {
		const { user } = this.state
		user[key] = event
		this.setState({ user: this.state.user })
	}

	/**
	* submit forgot password data
	* @param email
	* @param password
	* @param confirm password
	*/
	onSubmit() {
		let fields = ['email', 'password', 'confirm_password']
		let { user } = this.state;
		let formValidation = IsValidForm(fields, user)
		this.setState({ errors: formValidation.errors })
		if (formValidation.validate) {
			if (user.password != user.confirm_password) {
				this.refs.toast.show("Password and confirm password doesn't match")
			}
			else {

				let data = { email: user.email, password: user.password }
				this.props.dispatch(editProfile(data)).then(res => {
					if (res.result == 'success') {
						this.refs.toast.show('Profile updated successful')
					}
					else {
						return (this.refs.toast.show(res.message))
					}
				})
			}
		} else {
			this.refs.toast.show('Some field are missing')
		}
	}

	/*Edit profile render screen function*/
	render() {
		return (
			<TouchableOpacity style={styles.container} onPress={Keyboard.dismiss} accessible={false} activeOpacity={1}>
				<View style={styles.innerCon}>
					<IconBar goBack={this.goBack.bind(this)} />
					<View style={styles.outerView}>
						<View style={styles.outerHeading}>
							<Text style={styles.text}>Edit Profile</Text>
						</View>
						<View style={styles.secondContainer}>
							<View style={styles.outerTextField}>
								<TextField focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="Email" onChange={this.onChange.bind(this, 'email')} />
								<TextField secureTextEntry={true} focusedTextInput={styles.focusedTextInput} focusedOutTextInput={styles.focusedOutTextInput} placeholder="New Password" onChange={this.onChange.bind(this, 'password')} />
								<TextField secureTextEntry={true} placeholder="Confirm New Password" focusedTextInput={styles.focusWithoutBorder} focusedOutTextInput={styles.focusWithoutBorder} onChange={this.onChange.bind(this, 'confirm_password')} />
							</View>
							<BlockButton buttonStyle={styles.lastButton} textStyle={styles.whiteColor} label="Change Setting" onPress={this.onSubmit.bind(this)} />
						</View>
					</View>
				</View>
				<Toast
					ref="toast"
					style={{ backgroundColor: '#000', width: 300 }}
					position='bottom'
					fadeInDuration={750}
					fadeOutDuration={5000}
					opacity={0.8}
					textStyle={{ color: '#fff' }}
				/>
			</TouchableOpacity>
		)
	}
}

export default connect(state => ({}, mapDispatch))(EditProfile);

const mapDispatch = (dispatch) => {
	const allActionProps = Object.assign({}, dispatch);
	return allActionProps;
}

/*Edit profile page css as json*/
const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	innerCon: {
		flex: 1
	},
	text: {
		fontSize: 26,
		textAlign: 'center',
		marginBottom: 20
	},
	outerView: {
		paddingLeft: 20,
		paddingRight: 20,
		flex: 1
	},
	outerHeading: {
		flex: 2,
		justifyContent: 'center'
	},
	whiteColor: {
		color: '#fff'
	},
	secondContainer: {
		flex: 4
	},
	lastButton: {
		marginBottom: 15,
		backgroundColor: '#979799',
		borderRadius: 0,
		marginBottom: 10
	},
	outerTextField: {
		backgroundColor: '#dedee5',
		paddingLeft: 15,
		paddingRight: 15
	},
	focusedTextInput: {
		height: 45,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10,
		borderColor: '#9df441'
	},
	focusedOutTextInput: {
		height: 45,
		borderBottomWidth: 0.5,
		paddingLeft: 10,
		paddingRight: 10
	},
	focusWithoutBorder: {
		height: 45,
		paddingLeft: 10,
		paddingRight: 10,
	}
})