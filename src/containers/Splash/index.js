import React from 'react'
import { View, AsyncStorage, Image, StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux';
export default class Splash extends React.Component{
	async componentWillMount(){
		let islogged = await AsyncStorage.getItem('isLoggedin')
		if(islogged){
			Actions.Main()
		}else{
			Actions.WelcomePage();
		}
	}
	render(){
		return(
			<View style={styles.main}>
				<View style={styles.outterCon}>
					<Image source={require('../../img/splash.png')} style={styles.img}/>
				</View>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	main:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	outterCon:{
		width: 100,
		height: 100
	},
	img:{
		width: '100%',
		height: '100%'
	}
})