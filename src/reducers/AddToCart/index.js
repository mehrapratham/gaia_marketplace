import initialState from '../initialState'
import { ADD_TO_CART_LIST, GET_CARTITEM_LIST } from '../../actions/types'
export default (state = initialState.CartData, action) => {
 switch (action.type) {
   case ADD_TO_CART_LIST:
    return { ...state, cart: action.data }
    case GET_CARTITEM_LIST:
    return { ...state, cartItemList: action.data }
   default:
    return state


    
 }
}