import initialState from '../initialState'
import { GET_CATEGORY_LIST, GET_BROWSEMARKET_LIST, GET_ITEMDETAIL_LIST, GET_INBOX_LIST, GET_ACCOUNT_LIST } from '../../actions/types'
export default (state = initialState.MarketPlace, action) => {
  switch (action.type) {
    case GET_CATEGORY_LIST:
      return { ...state, categoryList: action.data }
    case GET_BROWSEMARKET_LIST:
      return { ...state, browseMarketList: action.data }
    case GET_ITEMDETAIL_LIST:
      return { ...state, itemDetailList: action.data }
    case GET_INBOX_LIST:
      return { ...state, inboxList: action.data }
    case GET_ACCOUNT_LIST:
      return { ...state, account: action.data }
    default:
      return state
  }
}