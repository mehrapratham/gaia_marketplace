import { combineReducers } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import MarketPlace from './MarketPlace';
import CartData from './AddToCart';
/*combine reducers*/
export default combineReducers({
 router: routerReducer,
 MarketPlace: MarketPlace,
 CartData
});