import React from 'react'
import { View, Text, StyleSheet, ScrollView, Image, Dimensions } from 'react-native'
import { Icon } from 'native-base';
var { height, width } = Dimensions.get('window');
export default class GameCards extends React.Component {
	getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	render() {
		return (
			<View style={styles.main}>
				<ScrollView horizontal={true} style={styles.scroll} showsHorizontalScrollIndicator={false}>
					{this.props.list.map((item, key) => {
						return (
							<View style={styles.conMain} key={key}>
								<View style={[styles.imgCon, { backgroundColor: this.getRandomColor() }]}>
									<View style={styles.imgOut}>
										<Image source={{ uri: item.thumbfile }} style={styles.imgSize} />
									</View>
								</View>
								<View style={styles.con}>
									<View style={styles.textOut}>
										<Text style={styles.textMain} numberOfLines={1}>{item.name}</Text>
									</View>
									<View style={styles.innerCon}>
										<View style={styles.priceOut}>
											<Text style={styles.textMain}>${item.price}</Text>
										</View>
										<View style={styles.like}>
											<Text style={styles.textMain}>{item.like + " "}<Icon name="ios-heart" style={styles.iconHeart} /></Text>
										</View>
									</View>
								</View>
							</View>
						)
					})}
				</ScrollView>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	main: {
		width: '100%'
	},
	scroll: {
		paddingBottom: 5,
		width: '100%'
	},
	imgOut: {
		height: 70,
		width: 70,
		overflow: 'hidden',
		borderRadius: 10
	},
	imgSize: {
		width: '100%',
		height: '100%'
	},
	con: {
		padding: 10,
		backgroundColor: '#fff'
	},
	textOut: {
		marginBottom: 5
	},
	textMain: {
		color: 'rgba(0,0,0,0.3)',
		fontSize: 12
	},
	innerCon: {
		flexDirection: 'row'
	},
	priceOut: {
		width: '50%'
	},
	like: {
		width: '50%',
		alignItems: 'flex-end',
	},
	imgCon: {
		paddingTop: 30,
		paddingBottom: 30,
		alignItems: 'center',
		justifyContent: 'center'
	},
	conMain: {
		borderRadius: 10,
		overflow: 'hidden',
		marginRight: 10,
		width: 130
	},
	iconHeart: {
		fontSize: 12,
		color: 'rgba(0,0,0,0.3)',
	},
})