import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
export default class ItemList extends React.Component {
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    render() {
        return (
            <TouchableOpacity style={styles.imgout} onPress={this.props.onPress} activeOpacity={this.props.activeOpacity}>
                <View style={[styles.imgcont, { backgroundColor: this.getRandomColor() }]}>
                    <View style={styles.imgmain}>
                        <Image source={{ uri: this.props.image }} style={styles.image} />
                    </View>
                </View>
                <View style={styles.textout}>
                    <Text style={styles.textDetails} numberOfLines={1}>{this.props.name}</Text>
                    <Text style={styles.textdetail}>Rarity:{this.props.rarity}</Text>
                    <Text style={styles.textdetail}>{this.props.date}</Text>
                </View>
                <View style={styles.touchableOut}>
                    <View style={styles.priceOut}>
                        <Text style={styles.price}>$ {this.props.price} </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({

    textDetails: {
        fontSize: 16,
        paddingBottom: 4
    },
    priceOut: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    price: {
        fontSize: 16
    },

    map: {
        paddingLeft: 10,
        marginBottom: 80,
        paddingRight: 10,
    },
    imgout: {
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        height:'auto'
    },
    imgcont: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        height:70,
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 10,
        position:'absolute',
        left:0,
        top:5
    },
    imgmain: {
        height: 60,
        width: 60,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    textout: {
        paddingLeft: 80,
        paddingRight:60,
        justifyContent: 'center',
    },
    textdetail: {
        fontSize: 12,
        paddingBottom: 3,
        color: 'rgba(0,0,0,0.4)'
    },
    touchableOut: {
       position:'absolute',
       right:0,
        top:30

    },

})