import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { Icon } from 'native-base'
export default class MenuList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedFilter: [1, 5, 8, 12, 14],
			list: [
				{
					heading: 'SORT BY',
					priceList: [
						{
							id: 1,
							name: 'Latest'
						},
						{
							id: 2,
							name: 'Lowest Price'
						},
						{
							id: 3,
							name: 'Highest Price'
						}
					]
				},
				{
					heading: 'GAME',
					priceList: [
						{
							id: 4,
							name: 'Witch Hazel'
						},
						{
							id: 5,
							name: 'Lake Kindred'
						},
						{
							id: 6,
							name: 'Quest Master'
						}
					]
				},
				{
					heading: 'TYPE',
					priceList: [
						{
							id: 7,
							name: 'Weapons'
						},
						{
							id: 8,
							name: 'Clothing'
						},
						{
							id: 9,
							name: 'Accessories'
						}
					]
				},
				{
					heading: 'TYPE',
					priceList: [
						{
							id: 10,
							name: 'Weapons'
						},
						{
							id: 11,
							name: 'Clothing'
						},
						{
							id: 12,
							name: 'Accessories'
						}
					]
				},
				{
					heading: 'TYPE',
					priceList: [
						{
							id: 13,
							name: 'Weapons'
						},
						{
							id: 14,
							name: 'Clothing'
						},
						{
							id: 15,
							name: 'Accessories'
						}
					]
				}
			]
		}
	}

	render() {
		var { height, width } = Dimensions.get('window');
		return (
			<View style={styles.main}>
				<View style={styles.container}>
					<View style={styles.width50}>
						<Text style={styles.textFilter}>Filter</Text>
					</View>
					<TouchableOpacity style={styles.width50} onPress={this.props.onPressDone}>
						<Text style={styles.TextDone}>Done</Text>
					</TouchableOpacity>
				</View>
				<View style={styles.scroll}>
					<ScrollView style={{ height: height - 70 }}>
						{this.state.list.map((item, key) => {
							return (
								<View key={key}>
									<View style={styles.text}>
										<Text style={{ fontSize: 17 }}>{item.heading}</Text>
									</View>
									{item.priceList.map((list, index) => {
										return (
											<TouchableOpacity key={index} style={styles.touchCon} >
												<View style={styles.conOut}>
													<Text style={styles.textColor}>{list.name}</Text>
												</View>
												{this.state.selectedFilter.indexOf(list.id) != -1 && <View style={styles.width50}>
													<Icon name="ios-checkmark" style={styles.icon} />
												</View>}
											</TouchableOpacity>
										)
									})}
								</View>
							)
						})}
					</ScrollView>
				</View>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	main: {
		flex: 1,
		paddingTop: 10
	},
	container: {
		padding: 20,
		width: '100%',
		flexDirection: 'row',
		borderBottomWidth: 0.5,
		borderColor: 'rgba(0,0,0,0.2)'
	},
	width50: {
		width: '50%'
	},
	textFilter: {
		fontSize: 16
	},
	TextDone: {
		color: '#6580ea',
		alignSelf: 'flex-end',
		fontSize: 16
	},
	scroll: {
		paddingBottom: 20
	},
	text: {
		paddingLeft: 20,
		paddingBottom: 10,
		paddingTop: 20
	},
	touchCon: {
		flexDirection: 'row',
		width: '100%',
		paddingLeft: 20,
		paddingRight: 20,
		height: 30
	},
	conOut: {
		width: '50%',
		justifyContent: 'center'
	},
	textColor: {
		color: 'rgba(0,0,0,0.5)'
	},
	icon: {
		color: '#6580ea',
		alignSelf: 'flex-end'
	}
})