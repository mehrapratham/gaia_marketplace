import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native'
var { height, width } = Dimensions.get('window');
import PopupDialog from 'react-native-popup-dialog';
import GradientButton from '../Buttons/GradientButton';

export default class PopupComp extends React.Component {
    render() {
        return (
            <PopupDialog
                ref={this.props.link}
                width={width - 50}
                height={180}>
                <View style={styles.main}>
                    <View style={styles.con}>
                        <Text style={styles.textSize}>
                            {this.props.label}
                        </Text>
                    </View>
                    <View style={styles.textDescriptionOut}>
                        <Text style={styles.textDescription}>
                            {this.props.description}
                        </Text>
                    </View>
                    <View style={styles.btnGradient}>
                        <GradientButton label={this.props.btnText} onPress={this.props.onPress} />
                    </View>
                </View>
            </PopupDialog>
        )
    }
}
const styles = StyleSheet.create({

    main: {
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1
    },
    con: {
        flex: 1,
        justifyContent: 'center'
    },
    textSize: {
        fontSize: 22
    },
    textDescriptionOut: {
        flex: 1
    },
    textDescription: {
        fontSize: 12,
        color: 'grey',
        textAlign: 'center'
    },
    btnGradient: {
        width: '100%',
        flex: 1
    }
})