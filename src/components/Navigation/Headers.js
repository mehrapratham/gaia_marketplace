import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base'
export default class Headers extends React.Component {
    render() {
        return (
            <SafeAreaView style={styles.firstCon}>
                <View style={styles.secondCon}>
                    <View style={styles.iconback}>
                        <TouchableOpacity onPress={() => { Actions.pop() }}>
                            <Icon name={this.props.lefticon} style={styles.icon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.text}>
                        <Text style={styles.textmain}>{this.props.label}</Text>
                    </View>
                    <TouchableOpacity style={styles.iconcart} onPress={this.props.onPress}>
                        <Icon name={this.props.righticon} style={styles.iconCartMain} />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    icon: {
        fontSize: 24,
    },
    iconCartMain: {
        fontSize: 23,
    },
    iconcart: {
        alignItems: 'flex-end',
        width: '20%',
        paddingRight: 15
    },
    firstCon: {
        paddingTop: 34,
        marginBottom: 10,
        backgroundColor: '#fff',
        //height:100,
        // alignItems: 'flex-end',
        //marginTop:-30
    },
    secondCon:{
        paddingTop: 5, 
        flexDirection: 'row',
        width: '100%',
        marginBottom: 10,
        paddingBottom: 5,
        backgroundColor: '#fff',
    },
    iconback: {
        width: '20%',
        paddingLeft: 15
    },
    textmain: {
        fontSize: 16,

    },
    text: {
        width: '60%',
        alignItems: 'center',
        marginBottom: 5
    },
})