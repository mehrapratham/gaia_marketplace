import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native'
import { Icon, Badge } from 'native-base'
import { Actions } from 'react-native-router-flux';

export default class BadgeHeader extends React.Component {
    render() {
        return (
            <SafeAreaView style={this.props.firstCon}>
                <View style={styles.secondCon}>
                    <View style={styles.iconback}>
                        <TouchableOpacity onPress={() => { Actions.pop() }}>
                            <Icon name={this.props.lefticon} style={styles.icon} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.text}>
                        <Text style={styles.textmain} numberOfLines={1}>{this.props.label}</Text>
                    </View>
                    <TouchableOpacity style={styles.iconcart} onPress={this.props.onPress}>
                        <View style={styles.badgeOut}>
                            <Badge style={styles.badgeMain}>
                                <Text style={styles.badgeIcon}>{this.props.badgeNo}</Text>
                            </Badge>
                            <Icon name={this.props.righticon} style={styles.icon} />
                        </View>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    secondCon: {
        paddingTop: 5,
        flexDirection: 'row',
        width: '100%',
        marginBottom: 10,
        paddingBottom: 5,
        paddingLeft: 15,
        paddingRight: 15,
       // backgroundColor: '#fff',
    },
    icon: {
        fontSize: 24
    },
    iconcart: {
        alignItems: 'flex-end',
        width: '20%'
    },
    // firstCon: {
    //     flexDirection: 'row',
    //     width: '100%',
    //     position:'absolute',
    //     top:10
    // },
    iconback: {
        width: '20%',
    },
    textmain: {
        fontSize: 16,
    },
    text: {
        width: '60%',
        alignItems: 'center',
    },
    badgeOut: {
        flexDirection: 'row'
    },
    badgeMain: {
        backgroundColor: 'red',
        height: 18,
        width: 18,
        borderRadius: 15,
        marginTop: 4,
        alignItems: 'center'
    },
    badgeIcon: {
        color: '#fff',
        fontSize: 12
    },
})