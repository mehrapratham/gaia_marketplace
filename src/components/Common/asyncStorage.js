import {AsyncStorage } from 'react-native'

export const saveToAsyncStorage = (key, data) => {
   return dispatch => {
     try {
        let stringObject = JSON.stringify(data)
        	AsyncStorage.setItem(key,stringObject)
	    } catch (error) {
	        console.log(error)
	    }
   }
}
export const getFromAsyncStorage = (key) => {
   return async dispatch => {
	  	try {
		    let data = await AsyncStorage.getItem(key);
		   	data = JSON.parse(data) || [];
		    if (data.length != 0) {
		    	return data
		    }
		    else{
		    	return null
		    }
		}
		catch(error){
			console.log(error)
		}
	}
}
export const removeAsyncStorage = (key) => {
   return dispatch => {
     AsyncStorage.removeItem(key)
   }
}