import React from 'react'
import Toast from 'react-native-easy-toast'
export default class ToastComponent extends React.Component{
	render(){
		return(
			<Toast 
	        	ref={this.props.ref}
	        	style={{backgroundColor:'#000',width: 300}}
                position='bottom'
                fadeInDuration={750}
                fadeOutDuration={5000}
                opacity={0.8}
                textStyle={{color:'#fff'}}
	        />
		)
	}
}