import React from 'react'
import { TextInput, Keyboard } from 'react-native';
export default class TextField extends React.Component {
	constructor(props) {
		super(props);
		this.state = { hasFocus: false };
	}
	onChange(event) {
		this.props.onChange(event)
	}
	setFocus(hasFocus, e) {
		this.setState({ hasFocus });
		if (hasFocus && this.props.onFocus) {
			this.props.onFocus(true)
			console.log("1111")
		} else if (!hasFocus && this.props.onFocus) {
			console.log("22222")
			this.props.onFocus(false)
		}
	}
	render() {
		return (
			<TextInput
				placeholder={this.props.placeholder}
				placeholderTextColor="#646262"
				style={this.state.hasFocus ? this.props.focusedTextInput : this.props.focusedOutTextInput}
				value={this.props.value}
				returnKeyType={this.props.returnKeyType}
				underlineColorAndroid="transparent"
				onFocus={this.setFocus.bind(this, true)}
				onBlur={this.setFocus.bind(this, false)}
				onChangeText={this.props.onChange}
				keyboardType={this.props.keyboardType}
				secureTextEntry={this.props.secureTextEntry}
			/>
		)
	}
}
