import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo';
export default class GradientButton extends React.Component {
	render() {
		return (
			<TouchableOpacity onPress={this.props.onPress} disabled={this.props.disable}>
				<LinearGradient
					colors={['#66e8dc', '#6580ea']}
					style={styles.container}
					start={[0.1, 0.1]} end={[1, 0.3]}
				>
					<Text style={styles.color}>{this.props.label}</Text>
				</LinearGradient>
			</TouchableOpacity>
		)
	}
}
const styles = StyleSheet.create({
	container: {
		height: 50,
		borderRadius: 10,
		alignItems: 'center',
		justifyContent: 'center',
	},
	color: {
		color: '#fff'
	}
})