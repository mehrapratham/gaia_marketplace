import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import WelcomePage from './containers/WelcomePage';
import Login from './containers/Auth/Login';
import LoginWithFb from './containers/Auth/LoginWithFb';
import Signup from './containers/Auth/Signup';
import ForgotPassword from './containers/Auth/ForgotPassword';
import ResetPassword from './containers/Auth/ResetPassword';
import EditProfile from './containers/Auth/EditProfile';
import Splash from './containers/Splash';
import Main from './containers/Main';
import IconDetailPage from './containers/Main/MarketPlace/IconDetailPage';
import CartItem from './containers/Main/CartItem';
import ItemDetailPage from './containers/Main/MarketPlace/ItemDetailPage';
import Purchases from './containers/Main/Accounts/Purchases';
import Selling from './containers/Main/Accounts/Selling';
import Profile from './containers/Main/Accounts/Profile';
import SellingDetailPage from './containers/Main/Accounts/Selling/SellingDetailPage';
import SearchDetails from './containers/Main/SearchDetails';
import Payment from './containers/Main/MarketPlace/Payment';
import InventoryDetail from './containers/Main/Inventory/InventoryDetail';
import SellItem from './containers/Main/Inventory/SellItem';
import AddFunds from './containers/Main/Accounts/AddFunds';
import AddSource from './containers/Main/Accounts/AddFunds/AddSource';
import PurchaseDetails from './containers/Main/Accounts/Purchases/PurchaseDetails';
import SellingItem from './containers/Main/Accounts/Purchases/SellingItem';
import Accounts from './containers/Main/Accounts';
import {BackHandler,Alert} from 'react-native'
import {Actions} from 'react-native-router-flux'
import Inbox from './containers/Main/Inbox/index';
import Inventory from './containers/Main/Inventory';
import ChangePassword from './containers/Auth/ChangePassword';
import RemoveFunds from './containers/Main/Accounts/RemoveFunds';



export default class App extends Component {
  componentDidMount () {

    const disableButtonOnScenes = [
      // 'Home',
      'Main'
    ]
  
    BackHandler.addEventListener('hardwareBackPress', function() {

      if(!disableButtonOnScenes.find(scene => scene == Actions.currentScene)) {
        console.log(1212)
        return false;
      }
      console.log("bahr gya yrr")
      
      Alert.alert(
        'Exit App',
        'Exiting the application?', [{
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
        }, {
            text: 'OK',
            onPress: () => BackHandler.exitApp()
        }, ], {
            cancelable: false
        }
     )
      return true;
    });
  }
  /*render function for main App screen*/
  render() {
    console.disableYellowBox = true;
    return (
      /*Routes*/
      <Router hideNavBar= "true" >
        <Scene key="root" duration={0}>
          <Scene key="Splash" component={Splash} hideNavBar initial={true}/>
          <Scene key="WelcomePage" component={WelcomePage} hideNavBar />
          <Scene key="Login" component={Login} hideNavBar />
          <Scene key="LoginWithFb" component={LoginWithFb} hideNavBar />
          <Scene key="Signup" component={Signup} hideNavBar />
          <Scene key="ForgotPassword" component={ForgotPassword} hideNavBar />
          <Scene key="ResetPassword" component={ResetPassword} hideNavBar />
          <Scene key="EditProfile" component={EditProfile} hideNavBar />
          <Scene key="Main" component={Main} hideNavBar />
          <Scene key="IconDetailPage" component={IconDetailPage} hideNavBar />
          <Scene key="Purchases" component={Purchases} hideNavBar />
          <Scene key="SellingDetailPage" component={SellingDetailPage} hideNavBar />
          <Scene key="Selling" component={Selling} hideNavBar />
          <Scene key="ItemDetailPage" component={ItemDetailPage} hideNavBar/>
          <Scene key="CartItem" component={CartItem} hideNavBar/>
          <Scene key="Profile" component={Profile} hideNavBar/>
          <Scene key="SearchDetails" component={SearchDetails} hideNavBar/>
          <Scene key="Payment" component={Payment} hideNavBar/>
          <Scene key="InventoryDetail" component={InventoryDetail} hideNavBar/>
          <Scene key="SellItem" component={SellItem} hideNavBar/>
          <Scene key="AddFunds" component={AddFunds} hideNavBar/>
          <Scene key="AddSource" component={AddSource} hideNavBar/>
          <Scene key="PurchaseDetails" component={PurchaseDetails} hideNavBar/>
          <Scene key="SellingItem" component={SellingItem} hideNavBar/>
          <Scene key="Accounts" component={Accounts} hideNavBar/>
          <Scene key="Inbox" component={Inbox} hideNavBar />
          <Scene key="Inventory" component={Inventory} hideNavBar />
          <Scene key="ChangePassword" component={ChangePassword} hideNavBar />
          <Scene key="RemoveFunds" component={RemoveFunds} hideNavBar />
        </Scene>
      </Router>
    )
  }
}