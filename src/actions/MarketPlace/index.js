import axios from 'axios'
import { CATEGORY_URL, BROWSEMARKET_URL, ITEMDETAIL_URL, PURCHASE_URL, INBOX_URL, SELLING_URL, SELLINGLIST_URL, CARTITEM_URL, CHECKOUT_URL, PURCHASELIST_URL, TRANSFER_URL, ACCOUNT_URL, CHANGEPASSWORD_URL, ADDBALANCE_URL, REMOVE_URL } from '../apiConstant'
import { HAS_ERROR, GET_CATEGORY_LIST, GET_BROWSEMARKET_LIST, GET_SELLING_LIST, GET_ITEMDETAIL_LIST, ADD_TO_CART_LIST, GET_PURHASE_LIST, GET_INBOX_LIST, GET_SELLINGLIST_LIST, GET_CARTITEM_LIST, GET_CHECKOUT_LIST, GET_PURCHASELIST_LIST, GET_TRANSFER_LIST, GET_ACCOUNT_LIST, GET_CHANGEPASSWORD_LIST, GET_ADDBALANCE_LIST, GET_REMOVE_LIST } from '../types'
import { AsyncStorage } from 'react-native'
import { saveToAsyncStorage, getFromAsyncStorage } from '../../components/Common/asyncStorage'
import store from '../../store'
let { dispatch, getState } = store;
export const categoryList = () => {
  return dispatch => {
    return axios
      .get(`${CATEGORY_URL}`, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        console.log(res.data.data,"action")
        dispatch({
          type: GET_CATEGORY_LIST,
          data: res.data.data,
        })
        return res.data.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const browseMarket = (params) => {
  return dispatch => {
    return axios
      .get(`${BROWSEMARKET_URL}?category=` + params, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_BROWSEMARKET_LIST,
          data: res.data.data,
        })
        return res.data.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const itemDetail = (params) => {
  return dispatch => {
    return axios
      .get(`${ITEMDETAIL_URL}?id=` + params, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_ITEMDETAIL_LIST,
          data: res.data.data,
        })
        return res.data.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}
export const CartItems = (itemId, mode) => {
  return dispatch => {
    return axios
      .get(`${CARTITEM_URL}?id=` + itemId + `&mode=` + mode, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        let CartListArray = [];
        if (mode === "show") {
          let CartList = res.data.data;
          let CartListKeys = Object.keys(res.data.data)
          for (let i = 0; i < CartListKeys.length; i++) {
            let key = CartListKeys[i];
            CartListArray.push(CartList[key])
          }
        } else if (mode === "remove") {
          let cartList = getState()
          cartList = cartList.CartData.cartItemList
          //let index = cartList.map(function (x) { return x.id; }).indexOf(itemId);
          let index = cartList.findIndex(val => val.id == itemId)
          cartList.splice(index, 1)
          CartListArray = cartList
        }
        dispatch({
          type: GET_CARTITEM_LIST,
          data: CartListArray,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}
// export const showCart = (params) => {
//   return dispatch => {
//     return axios
//       .get(`${SHOWCART_URL}`, {
//         headers: { 'Content-Type': 'application/json' }
//       })
//       .then(res => {
//         dispatch({
//           type: GET_SHOWCART_LIST,
//           data: res.data,
//         })
//         return res.data
//       })
//       .catch(function (error) {
//         dispatch({
//           type: HAS_ERROR,
//           data: error,
//         })
//         return error
//       })
//   }
// }


// export const addToCart = (data) => {
//   let cartList = getState().CartData.cart;
//   cartList.push(data)
//   dispatch(saveToAsyncStorage('carts', cartList))
//   return dispatch => {
//     return dispatch({
//       type: ADD_TO_CART_LIST,
//       data: cartList,
//     })
//   }
// }

export const initialLoad = () => {
  return async dispatch => {
    let data = await dispatch(getFromAsyncStorage('carts'))
    return dispatch({
      type: ADD_TO_CART_LIST,
      data: data || [],
    })
  }
}
// export const removeFromCart = (id) => {
//   let cartList = getState().CartData.cart;
//   let index = cartList.map(function (cart) { return cart.id; }).indexOf(id)
//   cartList.splice(index, 1)
//   console.log(index, 33, cartList)
//   dispatch(saveToAsyncStorage('carts', cartList))
//   return dispatch({
//     type: ADD_TO_CART_LIST,
//     data: cartList,
//   })
// }

export const purchase = (params) => {
  return dispatch => {
    return axios
      .get(`${PURCHASE_URL}?id=` + params, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_PURHASE_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const purchasehistory = () => {
  return dispatch => {
    return axios
      .get(`${PURCHASELIST_URL}`, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_PURCHASELIST_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}
export const sellings = (serialid, price, start, end, category) => {
  return dispatch => {
    return axios
      .get(`${SELLING_URL}?serialid=` + serialid + `&start=` + start + `&end=` + end + `&category=` + category + `&price=` + price, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        const itemId = res.data.data.item_id
        let purchaseList = getState()
        purchaseList = purchaseList.MarketPlace.inboxList
        let index = purchaseList.map(function (x) { return x.item_id; }).indexOf(itemId);
        purchaseList.splice(index, 1)
        dispatch({
          type: GET_SELLING_LIST,
          data: purchaseList,
        })
        return res.data.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const InboxList = (params) => {
  console.log(params, "666666666")
  return dispatch => {
    return axios
      .get(`${INBOX_URL}?mode=` + params, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_INBOX_LIST,
          data: res.data.data,
        })
        return res.data.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const sellingList = (params) => {
  console.log(params, "666666666")
  return dispatch => {
    return axios
      .get(`${SELLINGLIST_URL}?mode=` + params, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_SELLINGLIST_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}
export const checkout = (params) => {
  return dispatch => {
    return axios
      .get(`${CHECKOUT_URL}`, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_CHECKOUT_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const transfer = (serial,category) => {
  return dispatch => {
    return axios
      .get(`${TRANSFER_URL}?serialid=`+serial+`&category=`+category, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        console.log(res.data,"resss00000")
        dispatch({
          type: GET_TRANSFER_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const accountDetails = (serial,category) => {
  return dispatch => {
    return axios
      .get(`${ACCOUNT_URL}`, {
        headers: { 'Content-Type': 'application/json' }
      })
      .then(res => {
        dispatch({
          type: GET_ACCOUNT_LIST,
          data: res.data.data,
        })
        return res.data.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const changePass = (email, oldpassword, newpassword) => {
  return dispatch => {
    let data = {email: email, oldpassword: oldpassword, newpassword: newpassword}
    return axios
      .post(`${CHANGEPASSWORD_URL}`, data)
      .then(res => {
        dispatch({
          type: GET_CHANGEPASSWORD_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

export const addBalance = (data) => {
  return dispatch => {
    return axios
      .post(`${ADDBALANCE_URL}?amount=` +data)
      .then(res => {
        dispatch({
          type: GET_ADDBALANCE_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}
export const removebalance = (data) => {
  return dispatch => {
    return axios
      .post(`${REMOVE_URL}?amount=` +data)
      .then(res => {
        dispatch({
          type: GET_REMOVE_LIST,
          data: res.data,
        })
        return res.data
      })
      .catch(function (error) {
        dispatch({
          type: HAS_ERROR,
          data: error,
        })
        return error
      })
  }
}

