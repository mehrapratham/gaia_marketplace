const dev_server = 'https://www.gaiaonline.com/api/marketplace';

/**
* Base URL for all the APIs
*/
const BaseUrl = dev_server;

/**
* API Constanst for different actions which are used to call an particular API
*/
// export const BASE_URL                =           BaseUrl+':3000/'
export const SIGNUP_URL              =           BaseUrl+'/register'
export const LOGIN_URL               =           BaseUrl+'/login'
export const LOGOUT_URL              =           BaseUrl+'/logout'
export const LOST_PASSWORD           =           BaseUrl+'/lostpassword'
export const REST_PASSWORD           =           BaseUrl+'/passwordrecover'
export const UPDATE_PROFILE          =           BaseUrl+'/account'
export const CATEGORY_URL            =           BaseUrl+'/category'
export const BROWSEMARKET_URL   	 =           BaseUrl+'/browsemarket'
export const ITEMDETAIL_URL   	 	 =           BaseUrl+'/ItemDetail'
export const PURCHASE_URL   	 	 =           BaseUrl+'/purchase'
export const PURCHASELIST_URL   	 =           BaseUrl+'/purchasehistory'
export const SELLING_URL   	 	     =           BaseUrl+'/sell'
export const SELLINGLIST_URL   	 	 =           BaseUrl+'/sellhistory'
export const CARTITEM_URL   	     =           BaseUrl+'/cart'
export const CHECKOUT_URL   	     =           BaseUrl+'/checkout'
export const TRANSFER_URL   	     =           BaseUrl+'/MoveItem'
export const INBOX_URL   	         =           BaseUrl+'/inbox'
export const ACCOUNT_URL   	         =           BaseUrl+'/account'
export const CHANGEPASSWORD_URL   	 =           BaseUrl+'/PasswordChange'
export const ADDBALANCE_URL   	     =           BaseUrl+'/addbalance'
export const REMOVE_URL   	         =           BaseUrl+'/removebalance'


