import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Route from './src/Route'
import { ThemeProvider } from 'react-native-material-ui';
import { Provider } from 'react-redux';
import store from './src/store';
import Expo from 'expo'
export default class App extends React.Component {
  constructor(){
    super();
    this.state = {
      loading: false
    }
  }
  async componentDidMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState({ loading: true })
  }
  render() {

    return (
      <Provider store={store}>
        <ThemeProvider>
          <View style={styles.container}>
            {this.state.loading &&<Route />}
          </View>
        </ThemeProvider>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
