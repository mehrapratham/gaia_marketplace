### Tech

* [react native] - React native for mobile apps!


### Modules
* [expo] - A free and open source toolchain built around React Native to help you build native iOS and Android apps using JavaScript and React.
* [native-base] - NativeBase is an open source framework to build React Native apps over a single JavaScript codebase for Android and iOS.
* [react-native-easy-toast] - A react native module to show toast like android, it works on iOS and Android.
* [react-native-material-ui] - an open source project which aims to bring Material Design to Android through React Native by Facebook.
* [react-native-router-flux] - Based on latest React Navigation API;
* [react-native-vector-icons] - Customizable Icons for React Native with support for NavBar/TabBar/ToolbarAndroid
* [react-router-redux] - using for routing
* [redux-thunk] - Redux Thunk middleware allows you to write action creators that return a function instead of an action.

### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ cd marketplace
$ yarn install
$ yarn ios
```
